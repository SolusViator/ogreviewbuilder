#include "ResourcesList.h"

bool ResourcesList::parseConfigFile(Ogre::String fileName)
{
	meshes.clear();
	particles.clear();
	materials.clear();


	TiXmlDocument *XMLDoc = NULL;
	try{
		Ogre::String basename, path;
		Ogre::StringUtil::splitFilename(fileName, basename, path);
		Ogre::DataStreamPtr pStream = Ogre::ResourceGroupManager::getSingleton().openResource(basename);
		//DataStreamPtr pStream = ResourceGroupManager::getSingleton().
		//	openResource( SceneName, groupName );

		Ogre::String data = pStream->getAsString();
		// Open the .scene File
		XMLDoc = new TiXmlDocument();
		XMLDoc->Parse(data.c_str());
		pStream->close();
		pStream.setNull();

		if (XMLDoc->Error())
		{
			//We'll just log, and continue on gracefully
			Ogre::LogManager::getSingleton().logMessage("[ConfigFile] The TiXmlDocument reported an error");
			delete XMLDoc;
			return false;
		}
	}
	catch (...)
	{
		//We'll just log, and continue on gracefully
		Ogre::LogManager::getSingleton().logMessage("[ConfigFile] Error creating TiXmlDocument");
		delete XMLDoc;
		return false;
	}

	// Validate the File
	auto XMLRoot = XMLDoc->RootElement();
	if (Ogre::String(XMLRoot->Value()) != "resources") {
		Ogre::LogManager::getSingleton().logMessage("[ConfigFile] Error: Invalid .config File. Missing <resources>");
		delete XMLDoc;
		return false;
	}

	loadFile(XMLRoot);
	delete XMLDoc;
	return true;
}
void ResourcesList::loadFile(TiXmlElement* XMLRoot){
	
	auto pElement = XMLRoot->FirstChildElement("mesh");
	while (pElement){
		loadMeshInfo(pElement);
		pElement = pElement->NextSiblingElement("mesh");
	}

	pElement = XMLRoot->FirstChildElement("material");
	while (pElement){
		loadMaterialInfo(pElement);
		pElement = pElement->NextSiblingElement("mateiral");
	}

	// Laduje wszystkie efekty czasteczkowe dostepne w ParticleManager
	loadParticleInfo();

}

void ResourcesList::loadMeshInfo(TiXmlElement* pElement)
{
	Ogre::String ext, file, fileExt;
	ext = pElement->Attribute("extension");

	Ogre::FileInfoListPtr fileinfoiter = Ogre::ResourceGroupManager::getSingleton().listResourceFileInfo(Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME);
	for (unsigned int i = 0; i < (*fileinfoiter).size(); i++){
		Ogre::StringUtil::splitBaseFilename((*fileinfoiter)[i].filename, file, fileExt);
		if (ext.compare(fileExt) == 0){
			Ogre::LogManager::getSingleton().logMessage("\n File: " + file);
			meshes.push_back(file + "." + fileExt);
		}
	}
}
void ResourcesList::loadMaterialInfo(TiXmlElement* pElement)
{
	Ogre::String ext, file, fileExt;
	ext = pElement->Attribute("extension");

	Ogre::FileInfoListPtr fileinfoiter = Ogre::ResourceGroupManager::getSingleton().listResourceFileInfo(Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME);
	for (unsigned int i = 0; i < (*fileinfoiter).size(); i++){
		Ogre::StringUtil::splitBaseFilename((*fileinfoiter)[i].filename, file, fileExt);
		if (ext.compare(fileExt) == 0){
			materials.push_back(file + "." + fileExt);
		}
	}
}
void ResourcesList::loadParticleInfo()
{
	auto iterator = Ogre::ParticleSystemManager::getSingleton().getTemplateIterator();
	while (iterator.hasMoreElements()){
		auto x = iterator.getNext();
		Ogre::LogManager::getSingleton().logMessage("\n Particle : " + x->getName());
		particles.push_back(x->getName());
	}
}