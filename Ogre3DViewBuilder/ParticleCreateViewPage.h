#pragma once
#include "GuiPage.h"
#include "ResourcesList.h"
#include "MeshDate.h"
#include "MYGUI\MyGUI_ComboBox.h"
#include "GuiController.h"
#include "PointerController.h"

class ParticleCreateViewPage : public GuiPage
{
public:
	ParticleCreateViewPage(ResourcesList* resList, GuiController* guiController);
	~ParticleCreateViewPage();
	virtual void ShowGuiWindow();
	virtual void HideGuiWindow();
	MeshDate getParticleDate();
private:
	MyGUI::Window* _Window;
	MyGUI::EditBox* _PositionX;
	MyGUI::EditBox* _PositionY;
	MyGUI::EditBox* _PositionZ;
	MyGUI::ComboBox* _particlesComboBox;
protected:
	void loadParticles(ResourcesList* resList);
	void setCursorPosition(MyGUI::Widget* _widget);
};

