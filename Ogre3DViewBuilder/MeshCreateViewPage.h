#pragma once
#include "GuiPage.h"
#include "ResourcesList.h"
#include "MeshDate.h"
#include "GuiController.h"
#include "PointerController.h"
#include "SceneManager.h"

class MeshCreateViewPage : public GuiPage
{
public:
	MeshCreateViewPage(ResourcesList* resList, GuiController* guiController);
	virtual void ShowGuiWindow();
	virtual void HideGuiWindow();
	MeshDate getMeshDate();
	~MeshCreateViewPage();
private:
	MyGUI::Window* _Window;
	MyGUI::EditBox* _PositionX;
	MyGUI::EditBox* _PositionY;
	MyGUI::EditBox* _PositionZ;
	MyGUI::EditBox* _RotationX;
	MyGUI::EditBox* _RotationY;
	MyGUI::EditBox* _RotationZ;
	MyGUI::EditBox* _RotationW;
	MyGUI::EditBox* _ScaleX;
	MyGUI::EditBox* _ScaleY;
	MyGUI::EditBox* _ScaleZ;
	MyGUI::ComboBox* _meshesComboBox;
protected:
	void loadMeshes(ResourcesList* resList);
	void setCursorPosition(MyGUI::Widget* _widget);
	void parseScene(MyGUI::Widget* _widget);


	void DiffColorButtonPressed(MyGUI::Widget* _sender);
	void SpecColorButtonPressed(MyGUI::Widget* _sender);

};