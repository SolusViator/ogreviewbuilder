#pragma once
#include "GuiPage.h"
#include "Ogre.h"
#include "MeshDate.h"
#include "EditedObject.h"
#include <string>
#include "ColorPanelViewPage.h"
class MeshEditViewPage : public GuiPage
{
public:
	MeshEditViewPage();
	~MeshEditViewPage();
	virtual void ShowGuiWindow();
	virtual void HideGuiWindow();
	virtual void Update();
	void UpdateMeshViewDate(MeshDate date);
	MeshDate GetMeshDateFromView();
protected:
	void updateMeshPosition(Ogre::Vector3 position);
	void updateMeshOrientation(Ogre::Quaternion orientation);
	void updateMeshScale(Ogre::Vector3 scale);
	Ogre::Vector3 getMeshPosition();
	Ogre::Quaternion getMeshOrientation();
	Ogre::Vector3 getMeshScale();
	void UpdateEditedObject(MyGUI::EditBox* _sender);
	void UpdateEditedObjectColor(MyGUI::Widget* _sender, MyGUI::Widget* _new);
	std::string deleteNoNNumberCharacter(std::string string);
	std::string getTypeName(ObjectsMask mask);
	ObjectsMask getType(std::string typeName);
private:
	MyGUI::Window* _Window;
	MyGUI::EditBox* _PositionX;
	MyGUI::EditBox* _PositionY;
	MyGUI::EditBox* _PositionZ;

	MyGUI::EditBox* _OrientationX;
	MyGUI::EditBox* _OrientationY;
	MyGUI::EditBox* _OrientationZ;

	MyGUI::EditBox* _ScaleX;
	MyGUI::EditBox* _ScaleY;
	MyGUI::EditBox* _ScaleZ;

	MyGUI::Widget* _DiffColor;
	MyGUI::EditBox* _DiffColorR;
	MyGUI::EditBox* _DiffColorG;
	MyGUI::EditBox* _DiffColorB;

	MyGUI::Widget* _SpecColor;
	MyGUI::EditBox* _SpecColorR;
	MyGUI::EditBox* _SpecColorG;
	MyGUI::EditBox* _SpecColorB;

	MyGUI::EditBox* _SpotMid;
	MyGUI::EditBox* _SpotOutter;
	
	MyGUI::EditBox* _ObjectName;
	MyGUI::TextBox* _ObjectType;

	MyGUI::Widget* _EditPosition;
	MyGUI::Widget* _EditOrientation;
	MyGUI::Widget* _EditScale;
	MyGUI::Widget* _EditLightColor;
	MyGUI::Widget* _EditSpotlightRange;

	ColorPanelViewPage* _ColorPanelPage;

	void DiffColorButtonPressed(MyGUI::Widget* _sender);
	void SpecColorButtonPressed(MyGUI::Widget* _sender);
	MyGUI::Colour getDiffColour();
	MyGUI::Colour getSpecColour();
	void setDiffColour(Ogre::ColourValue colour);
	void setSpecColour(Ogre::ColourValue colour);
	Ogre::ColourValue changeMyGUItoOgreColour(MyGUI::Colour colour);
	MyGUI::Colour changeOgreToMyGuiColour(Ogre::ColourValue colour);

	template<typename T>
	T* initialiseByAttribute(std::string name)
	{
		return MyGUI::Gui::getInstancePtr()->findWidget<T>(name);
	}
};

