#include "Application.h"
#include "Camera.h"
#include "CameraController.h"
#include "InputListener.h"
#include "MeshManager.h"
#include "UserInterfaceManager.h"
#include "EditController.h"
Application::Application()
{
	_Engine = NULL;
	_ResourcesList = NULL;
}


Application::~Application()
{
}

void Application::RunApplication()
{
	_Engine = new Engine();
	_ResourcesList = new ResourcesList();
	_Engine->initialise();
	_ResourcesList->parseConfigFile("ResTypeConfig.xml");
	MeshManager* meshManager = new MeshManager(_Engine->getSceneManager());
	/***********************************************/
	_Engine->getSceneManager()->setAmbientLight(Ogre::ColourValue::Black);
	Camera* camera = new Camera(_Engine->getSceneManager(), _Engine->getRenderWindow());
	camera->CreateCamera("Kamerka", Ogre::ColourValue(0.125, 0.349, 0.659));
	CameraController* cameraController = new CameraController(camera);
	/***************************************************/
	Mesh3DController::getSingleton().setBaseSceneNode(meshManager->createSceneNode("Mesh3DController"));
	Mesh3DController::getSingleton().setMoveCursors(meshManager->createEntity("XArrow.mesh"), meshManager->createEntity("YArrow.mesh"), meshManager->createEntity("ZArrow.mesh"));
	Mesh3DController::getSingleton().setScaleCursors(meshManager->createEntity("ScaleCursorX.mesh"), meshManager->createEntity("ScaleCursorY.mesh"), meshManager->createEntity("ScaleCursorZ.mesh"));
	Mesh3DController::getSingleton().setRotateCursors(meshManager->createEntity("RotateCursorX.mesh"), meshManager->createEntity("RotateCursorY.mesh"), meshManager->createEntity("RotateCursorZ.mesh"));
	GuiController* guiController = new GuiController();
	UserInterfaceManager* userInterface = new UserInterfaceManager(_Engine->getRenderWindow(), _Engine->getSceneManager(),
		guiController, _ResourcesList, meshManager);
	EditController* editController = new EditController(new PointerController(meshManager->createEntityWithoutNode("Pointer.mesh")));
	InputListener* inputListener = new InputListener(cameraController, guiController, editController);
	_Engine->getKeyboard()->setEventCallback(inputListener);
	_Engine->getMouse()->setEventCallback(inputListener);
	/********************************************************/
	inputListener->mgr = _Engine->getSceneManager();
	/*********************************************************************/

	while (1){
		if (_Engine->getTimeSinceLastFrame() > (1 / 60.f)){
			_Engine->captureInputDevices();
			cameraController->UpdateCamera();
			userInterface->updateGui();
			editController->Update(cameraController->getCameraOrientatnion());
			_Engine->renderFrame();
		}
	}
}