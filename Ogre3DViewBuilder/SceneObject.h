#pragma once
#include "ObjectsMask.h"
#include "Ogre.h"
#include "MeshDate.h"
#include <vector>
#include "tinyxml.h"

class SceneObject
{
public:
	SceneObject(MeshDate date, SceneObject* parentObject = NULL);
	~SceneObject();
	Ogre::String getName();
	Ogre::String getMeshName();
	bool compareSceneNode(Ogre::SceneNode* node);
	void Update();
	SceneObject* findObject(Ogre::String Name);
	SceneObject* findObject(Ogre::SceneNode* Node);
	SceneObject* getParent();
	MeshDate getMeshDate();
	void addChildObject(SceneObject* childObject);
	void detachChild(SceneObject*);
	void setNewParent(SceneObject* parentObject);
	void setNewDate(MeshDate);
	bool validate();

	void CreateXmlSceneNode(TiXmlElement* rootNode);
protected:
	std::vector<SceneObject*> _childObjects;
	MeshDate _date;
	ObjectsMask _type;
	SceneObject* _parent;
	Ogre::SceneNode* _SceneNode;

	Ogre::String getLightType(Ogre::String type);
};

