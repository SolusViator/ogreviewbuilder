#include "Mesh3DController.h"

Mesh3DController* Mesh3DController::_instance = NULL;

Mesh3DController::Mesh3DController()
{
	_parentSceneNode = NULL;
	activeType = Axes::Type::Null;
}


Mesh3DController::~Mesh3DController()
{
}
void Mesh3DController::setBaseSceneNode(Ogre::SceneNode* node)
{
	if (node != NULL){
		_parentSceneNode = node;
		_parentSceneNode->setVisible(true);
	}
}

void Mesh3DController::setMoveCursors(Ogre::Entity* XAxis, Ogre::Entity* YAxis, Ogre::Entity* ZAxis)
{
	_MoveCursors.clear();
	if (XAxis && YAxis && ZAxis){
		XAxis->setQueryFlags(ArrowEditMask);
		XAxis->setRenderQueueGroup(Ogre::RENDER_QUEUE_9);
		YAxis->setQueryFlags(ArrowEditMask);
		YAxis->setRenderQueueGroup(Ogre::RENDER_QUEUE_9);
		ZAxis->setQueryFlags(ArrowEditMask);
		ZAxis->setRenderQueueGroup(Ogre::RENDER_QUEUE_9);

		float scale = 5;
		auto nodeX = _parentSceneNode->createChildSceneNode();
		nodeX->attachObject(XAxis);
		nodeX->setVisible(false);
		nodeX->setPosition(0.5*scale, 0, 0);
		nodeX->setScale(scale, 1, 1);
		_MoveCursors.push_back(nodeX);
		auto nodeY = _parentSceneNode->createChildSceneNode();
		nodeY->attachObject(YAxis);
		nodeY->setVisible(false);
		nodeY->setPosition(0, 0.5*scale, 0);
		nodeY->setScale(1, scale, 1);
		_MoveCursors.push_back(nodeY);
		auto nodeZ = _parentSceneNode->createChildSceneNode();
		nodeZ->attachObject(ZAxis);
		nodeZ->setVisible(false);
		nodeZ->setPosition(0, 0, -0.5*scale);
		nodeZ->setScale(1, 1, scale);
		_MoveCursors.push_back(nodeZ);
		_parentSceneNode->_update(true, true);
	}
}
void Mesh3DController::setRotateCursors(Ogre::Entity* XAxis, Ogre::Entity* YAxis, Ogre::Entity* ZAxis)
{
	_RotateCursors.clear();
	if (XAxis && YAxis && ZAxis){
		XAxis->setQueryFlags(ArrowEditMask);
		XAxis->setRenderQueueGroup(Ogre::RENDER_QUEUE_9);
		YAxis->setQueryFlags(ArrowEditMask);
		YAxis->setRenderQueueGroup(Ogre::RENDER_QUEUE_9);
		ZAxis->setQueryFlags(ArrowEditMask);
		ZAxis->setRenderQueueGroup(Ogre::RENDER_QUEUE_9);
		float scale = 4;
		auto nodeX = _parentSceneNode->createChildSceneNode();
		nodeX->attachObject(XAxis);
		nodeX->setVisible(false);
		nodeX->setPosition(0, 0, 0);
		nodeX->setScale(scale, scale, scale);
		_RotateCursors.push_back(nodeX);
		auto nodeY = _parentSceneNode->createChildSceneNode();
		nodeY->attachObject(YAxis);
		nodeY->setVisible(false);
		nodeY->setPosition(0, 0, 0);
		nodeY->setScale(scale, scale, scale);
		_RotateCursors.push_back(nodeY);
		auto nodeZ = _parentSceneNode->createChildSceneNode();
		nodeZ->attachObject(ZAxis);
		nodeZ->setVisible(false);
		nodeZ->setPosition(0, 0, 0);
		nodeZ->setScale(scale, scale, scale);
		_RotateCursors.push_back(nodeZ);
		_parentSceneNode->_update(true, true);
	}
}
void Mesh3DController::setScaleCursors(Ogre::Entity* XAxis, Ogre::Entity* YAxis, Ogre::Entity* ZAxis)
{
	_ScaleCursors.clear();
	if (XAxis && YAxis && ZAxis){
		XAxis->setQueryFlags(ArrowEditMask);
		XAxis->setRenderQueueGroup(Ogre::RENDER_QUEUE_9);
		YAxis->setQueryFlags(ArrowEditMask);
		YAxis->setRenderQueueGroup(Ogre::RENDER_QUEUE_9);
		ZAxis->setQueryFlags(ArrowEditMask);
		ZAxis->setRenderQueueGroup(Ogre::RENDER_QUEUE_9);
		float scale = 5;
		auto nodeX = _parentSceneNode->createChildSceneNode();
		nodeX->attachObject(XAxis);
		nodeX->setVisible(false);
		nodeX->setPosition(0.5*scale, 0, 0);
		nodeX->setScale(scale, 1, 1);
		_ScaleCursors.push_back(nodeX);
		auto nodeY = _parentSceneNode->createChildSceneNode();
		nodeY->attachObject(YAxis);
		nodeY->setVisible(false);
		nodeY->setPosition(0, 0.5*scale, 0);
		nodeY->setScale(1, scale, 1);
		_ScaleCursors.push_back(nodeY);
		auto nodeZ = _parentSceneNode->createChildSceneNode();
		nodeZ->attachObject(ZAxis);
		nodeZ->setVisible(false);
		nodeZ->setPosition(0, 0, -0.5*scale);
		nodeZ->setScale(1, 1, scale);
		_ScaleCursors.push_back(nodeZ);
		_parentSceneNode->_update(true, true);
	}
}

Mesh3DController& Mesh3DController::getSingleton()
{
	if (_instance == NULL){
		_instance = new Mesh3DController();
	}
	return *_instance;
}

void Mesh3DController::Update()
{
	if (activeType != Axes::Type::Null){
		if (_parentSceneNode != NULL){
			_parentSceneNode->setPosition(EditedObject::getSingleton().getObjectPosition());
			_parentSceneNode->setOrientation(EditedObject::getSingleton().getObjectOrientatnion());
			setCursorsScale(EditedObject::getSingleton().getObjectBoundingBoxSize());
		}
	}
}

void Mesh3DController::setCursorsScale(Ogre::Vector3 size)
{
	//size *= 1.4* EditedObject::getSingleton().getObjectScale();
	//if (activeType != Axes::Type::Null){
	//	auto iterator = activeType == Axes::Type::Move ? _MoveCursors.begin() : activeType == Axes::Type::Rotate ? _RotateCursors.begin() : _ScaleCursors.begin();
	//	(*iterator++)->setScale(Ogre::Vector3(size.x, 0.2*size.y, 0.2*size.z));
	//	(*iterator++)->setScale(Ogre::Vector3(0.2*size.x, size.y, 0.2*size.z));
	//	(*iterator)->setScale(Ogre::Vector3(0.2*size.x, 0.2*size.y, size.z));
	//}
}

void Mesh3DController::setVisibleType(Axes::Type type)
{
	if (_parentSceneNode != NULL && activeType != type){
		hide(activeType);
		activeType = type;
		if (activeType != Axes::Type::Null){
			show(activeType);
		}
		Update();
		_parentSceneNode->needUpdate(true);
	}
}

void Mesh3DController::hide(Axes::Type type)
{
	if (type != Axes::Null){
		auto iterator = activeType == Axes::Type::Move ? _MoveCursors.begin() : activeType == Axes::Type::Rotate ? _RotateCursors.begin() : _ScaleCursors.begin();
		auto endExt = activeType == Axes::Type::Move ? _MoveCursors.end() : activeType == Axes::Type::Rotate ? _RotateCursors.end() : _ScaleCursors.end();
		while (iterator != endExt)
		{
			(*iterator)->setVisible(false);
			iterator++;
		}
	}
}
void Mesh3DController::show(Axes::Type type)
{
	if (type != Axes::Null){
		auto iterator = activeType == Axes::Type::Move ? _MoveCursors.begin() : activeType == Axes::Type::Rotate ? _RotateCursors.begin() : _ScaleCursors.begin();
		auto endExt = activeType == Axes::Type::Move ? _MoveCursors.end() : activeType == Axes::Type::Rotate ? _RotateCursors.end() : _ScaleCursors.end();
		while (iterator != endExt)
		{
			(*iterator)->setVisible(true);
			iterator++;
		}
	}
}

Axes::Direction Mesh3DController::getAxe(Axes::Type type, Ogre::SceneNode* node)
{
	if (type != Axes::Null){
		auto iterator = activeType == Axes::Type::Move ? _MoveCursors.begin() : activeType == Axes::Type::Rotate ? _RotateCursors.begin() : _ScaleCursors.begin();
		auto endExt = activeType == Axes::Type::Move ? _MoveCursors.end() : activeType == Axes::Type::Rotate ? _RotateCursors.end() : _ScaleCursors.end();
		unsigned int a = 0;
		for (; iterator != endExt; a++, iterator++)
		{
			if ((*iterator) == node)break;
		}
		if (a < 3)return (Axes::Direction)a;
		else {
			return Axes::NoN;
		}
	}
	else{
		return Axes::NoN;
	}
}
