#pragma once
#include "SceneObject.h"
#include "Ogre.h"
#include <vector>

class SceneManager
{
public:
	static SceneManager& getSingleton();
	void addNewObject(SceneObject* object);
	SceneObject* findObject(Ogre::SceneNode* node);
	SceneObject* findObject(Ogre::String name);
	bool createSceneFile(Ogre::String fileName);
private:
	SceneManager();
	~SceneManager();
	std::vector<SceneObject*> _sceneTree;
	static SceneManager* _instance;
};

