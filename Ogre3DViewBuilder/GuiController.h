#pragma once
#include<map>
#include "MYGUI\MyGUI.h"
enum ChangePageView{
	MeshCreatePage = 1,
	Page2 = 1<<1
};

enum CreateObject{
	Mesh =1,
	ParticleEffect = 1<<1,
	Light = 1<<2
};

enum MaskType{
	CreateObjectMask = 1,
	ChangePageViewMask = 1<<1
};

class GuiController
{
public:
	GuiController();
	~GuiController();
	void CreateMeshButtonClicked(MyGUI::Widget* _widget);
	void CreateLightButtonClicked(MyGUI::Widget* _widget);
	void CreateParticleButtonClicked(MyGUI::Widget* _widget);
	void setMask(MaskType mask, unsigned int maskValue);
	unsigned int getMask(MaskType mask);
	void removeMask(MaskType mask, unsigned int maskValue);
protected:
	std::map<MaskType, unsigned int>_Masks;

};

