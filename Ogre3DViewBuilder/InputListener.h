#pragma once
#include "OISMouse.h"
#include "OISKeyboard.h"
#include "CameraController.h"
#include "GuiController.h"
#include "MYGUI\MyGUI.h"
#include "EditController.h"
#include "ObjectsMask.h"
enum SpecialKeys{
	Ctrl = 1,
	Alt = 1 << 1,
	Shift = 1<<2
};

class InputListener : public OIS::MouseListener, public OIS::KeyListener
{
public:
	InputListener(CameraController* controller, GuiController* guiController, EditController* editController);
	Ogre::SceneManager* mgr;
	~InputListener();
	virtual bool keyPressed(const OIS::KeyEvent&);
	virtual bool keyReleased(const OIS::KeyEvent&);
	virtual bool mouseMoved(const OIS::MouseEvent& me);
	virtual bool mousePressed(const OIS::MouseEvent& me, OIS::MouseButtonID id);
	virtual bool mouseReleased(const OIS::MouseEvent& me, OIS::MouseButtonID id);
private:
	CameraController* _Controller;
	GuiController* _GuiController;
	EditController* _EditController;
	unsigned int _SpecialKeysMask;
	void setMaskBit(unsigned int state);
	void removeMaskBit(unsigned int state);
	void GetMeshInformation(const Ogre::MeshPtr mesh,
		size_t &vertex_count,
		Ogre::Vector3* &vertices,
		size_t &index_count,
		unsigned long* &indices,
		const Ogre::Vector3 &position,
		const Ogre::Quaternion &orient,
		const Ogre::Vector3 &scale);
	Ogre::SceneNode* getSceneQueryResult(Ogre::Ray ray, unsigned int mask = ~(0), Ogre::Vector3& hitPoint = Ogre::Vector3(0,0,0));
};

