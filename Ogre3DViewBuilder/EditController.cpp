#include "EditController.h"


EditController::EditController(PointerController* pointerController)
{
	_pointerController = pointerController;
	_MeshEditViewPage = new MeshEditViewPage();
	isLeftButtonPressed = false;
	direction = Axes::NoN;
	type = Axes::Move;
}


EditController::~EditController()
{
}

void EditController::ChangeEditingObject(Ogre::SceneNode* node, Ogre::Ray ray , Ogre::Vector3 hitPoint )
{
	if (node)
	{
		if (EditedObject::getSingleton().getEditedObject() == node ){
			_pointerController->setPointerPosition(hitPoint);
		}
		else{
			auto object = SceneManager::getSingleton().findObject(node);
			if (object != NULL){
				EditedObject::getSingleton().setObject(object);
				_MeshEditViewPage->UpdateMeshViewDate(object->getMeshDate());
			}
			Mesh3DController::getSingleton().setVisibleType(Axes::Type::Scale);
		}
	}
	else{
		_pointerController->setPointerPosition(ray.getOrigin() + (ray.getDirection() * 10));
	}
}
void EditController::Update(Ogre::Quaternion cameraOrientatnion)
{
	if (EditedObject::getSingleton().getEditedObject() != NULL){
		if (Mesh3DController::getSingleton().getVisibleType() != type){
			Mesh3DController::getSingleton().setVisibleType(type);
		}
	}
	Mesh3DController::getSingleton().Update();
	if (direction != Axes::NoN && isLeftButtonPressed){
		Ogre::Vector3 dir = Ogre::Vector3::ZERO;
		Ogre::Vector3 cameraDir = Ogre::Vector3::ZERO;
		switch (direction)
		{
		case Axes::X:
			cameraDir = cameraOrientatnion* Ogre::Vector3::UNIT_X;
			if (Mesh3DController::getSingleton().getVisibleType() == Axes::Type::Rotate){
				EditedObject::getSingleton().getEditedObject()->pitch(Ogre::Radian((cameraDir.x*relMousePosition.x + cameraDir.y*relMousePosition.y)*-0.01));
			}
			else{
				dir = (EditedObject::getSingleton().getObjectOrientatnion()*Ogre::Vector3::UNIT_X)*0.05*(cameraDir.x*relMousePosition.x + cameraDir.y*relMousePosition.y);
			}
				break;
		case Axes::Y:
			cameraDir = -(cameraOrientatnion * Ogre::Vector3::UNIT_Y);
			if (Mesh3DController::getSingleton().getVisibleType() == Axes::Type::Rotate){
				EditedObject::getSingleton().getEditedObject()->roll(Ogre::Radian((cameraDir.x*relMousePosition.x + cameraDir.y*relMousePosition.y)*0.01));
			}
			else{
				dir = (EditedObject::getSingleton().getObjectOrientatnion()*Ogre::Vector3::UNIT_Y)*0.05*(cameraDir.y*relMousePosition.y);
			}
			break;
		case Axes::Z:
			cameraDir = (cameraOrientatnion * Ogre::Vector3::UNIT_Z);
			if (Mesh3DController::getSingleton().getVisibleType() == Axes::Type::Rotate){
				EditedObject::getSingleton().getEditedObject()->yaw(Ogre::Radian((cameraDir.x*relMousePosition.x + cameraDir.y*relMousePosition.y)*0.01));
			}
			else{
				dir = (EditedObject::getSingleton().getObjectOrientatnion()*Ogre::Vector3::UNIT_Z)*0.05*-(cameraDir.x*relMousePosition.x);
			}
			break;
		default:
			break;
		}
		if (Mesh3DController::getSingleton().getVisibleType() == Axes::Type::Move){
			EditedObject::getSingleton().setObjectPostion(EditedObject::getSingleton().getObjectPosition() + dir);
		}
		else if (Mesh3DController::getSingleton().getVisibleType() == Axes::Type::Scale){
			EditedObject::getSingleton().setObjectScale(EditedObject::getSingleton().getObjectScale() + dir);

		}
		relMousePosition = Ogre::Vector2::ZERO;
		EditedObject::getSingleton().updateObjectDate();
		_MeshEditViewPage->UpdateMeshViewDate(EditedObject::getSingleton().getObjectDate());
	}
}

void EditController::LeftButtonPressed(Ogre::SceneNode* node)
{
	isLeftButtonPressed = true;
	direction = Mesh3DController::getSingleton().getAxe(this->type, node);
}
void EditController::LeftButtonRelease()
{
	isLeftButtonPressed = false;
}
void EditController::setMouseRelPosition(Ogre::Vector2 relPosition)
{
	relMousePosition = relPosition;
}