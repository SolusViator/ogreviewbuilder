#pragma once
#include "Ogre.h"
#include "MeshDate.h"
#include "SceneManager.h"
class EditedObject
{
public:
	static EditedObject& getSingleton();
	void setObject(SceneObject* object);
	void setObjectPostion(Ogre::Vector3);
	void setObjectOrientation(Ogre::Quaternion);
	void setObjectScale(Ogre::Vector3);
	void updateObjectDate(MeshDate newDate);
	void updateObjectDate();
	MeshDate getObjectDate();
	Ogre::Vector3 getObjectPosition();
	Ogre::Quaternion getObjectOrientatnion();
	Ogre::Vector3 getObjectScale();
	Ogre::Vector3 getObjectBoundingBoxSize();
	Ogre::Vector3 getBoundingBoxCenterPosition();
	Ogre::SceneNode* getEditedObject(){ return _ObjectSceneNode; }
private:
	Ogre::SceneNode* _ObjectSceneNode;
	SceneObject* _object;
	static EditedObject* _editedObject;
	EditedObject();
	EditedObject(EditedObject&);
	~EditedObject();
};

