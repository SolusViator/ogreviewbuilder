#include "MeshEditViewPage.h"

using Ogre::StringConverter;
MeshEditViewPage::MeshEditViewPage()
{
	MyGUI::Gui* gui = MyGUI::Gui::getInstancePtr();
	MyGUI::LayoutManager::getInstancePtr()->loadLayout("EditMeshMenu.layout");
	_Window = gui->findWidget<MyGUI::Window>("_EditMeshWindow");
	//_PositionX = gui->findWidget<MyGUI::EditBox>("MeshEditPositionX");
	_PositionX = initialiseByAttribute<MyGUI::EditBox>("MeshEditPositionX"); 
	_PositionY = gui->findWidget<MyGUI::EditBox>("MeshEditPositionY");
	_PositionZ = gui->findWidget<MyGUI::EditBox>("MeshEditPositionZ");
	_OrientationX = gui->findWidget<MyGUI::EditBox>("MeshEditOrientationX");
	_OrientationY = gui->findWidget<MyGUI::EditBox>("MeshEditOrientationY");
	_OrientationZ = gui->findWidget<MyGUI::EditBox>("MeshEditOrientationZ");
	_ScaleX = gui->findWidget<MyGUI::EditBox>("MeshEditScaleX");
	_ScaleY = gui->findWidget<MyGUI::EditBox>("MeshEditScaleY");
	_ScaleZ = gui->findWidget<MyGUI::EditBox>("MeshEditScaleZ");
	_ObjectName = gui->findWidget<MyGUI::EditBox>("MeshEditName");
	_ObjectType = gui->findWidget<MyGUI::TextBox>("MeshEditType");

	_DiffColor = initialiseByAttribute<MyGUI::Widget>("LightEditDiffColor");
	_DiffColor->eventMouseButtonClick = MyGUI::newDelegate(this, &MeshEditViewPage::DiffColorButtonPressed);
	_DiffColorR = initialiseByAttribute<MyGUI::EditBox>("LightEditDiffColorR");
	_DiffColorG = initialiseByAttribute<MyGUI::EditBox>("LightEditDiffColorG");
	_DiffColorB = initialiseByAttribute<MyGUI::EditBox>("LightEditDiffColorB");

	_SpecColor = initialiseByAttribute<MyGUI::Widget>("LightEditSpecColor");
	_SpecColor->eventMouseButtonClick = MyGUI::newDelegate(this, &MeshEditViewPage::SpecColorButtonPressed);
	_SpecColorR = initialiseByAttribute<MyGUI::EditBox>("LightEditSpecColorR");
	_SpecColorG = initialiseByAttribute<MyGUI::EditBox>("LightEditSpecColorG");
	_SpecColorB = initialiseByAttribute<MyGUI::EditBox>("LightEditSpecColorB");

	_SpotMid = initialiseByAttribute<MyGUI::EditBox>("SpotlightEditMid");
	_SpotOutter = initialiseByAttribute<MyGUI::EditBox>("SpotlightEditOut");

	_EditPosition = initialiseByAttribute<MyGUI::Widget>("EditPosition");
	_EditOrientation = initialiseByAttribute<MyGUI::Widget>("EditOrientation");
	_EditScale = initialiseByAttribute<MyGUI::Widget>("EditScale");
	_EditLightColor = initialiseByAttribute<MyGUI::Widget>("EditLightColor");
	_EditSpotlightRange = initialiseByAttribute<MyGUI::Widget>("EditSpotRange");

	_PositionX->eventEditTextChange = MyGUI::newDelegate(this, &MeshEditViewPage::UpdateEditedObject);
	_PositionY->eventEditTextChange = MyGUI::newDelegate(this, &MeshEditViewPage::UpdateEditedObject);
	_PositionZ->eventEditTextChange = MyGUI::newDelegate(this, &MeshEditViewPage::UpdateEditedObject);
	_OrientationX->eventEditTextChange = MyGUI::newDelegate(this, &MeshEditViewPage::UpdateEditedObject);
	_OrientationY->eventEditTextChange = MyGUI::newDelegate(this, &MeshEditViewPage::UpdateEditedObject);
	_OrientationZ->eventEditTextChange = MyGUI::newDelegate(this, &MeshEditViewPage::UpdateEditedObject);
	_ScaleX->eventEditTextChange = MyGUI::newDelegate(this, &MeshEditViewPage::UpdateEditedObject);
	_ScaleY->eventEditTextChange = MyGUI::newDelegate(this, &MeshEditViewPage::UpdateEditedObject);
	_ScaleZ->eventEditTextChange = MyGUI::newDelegate(this, &MeshEditViewPage::UpdateEditedObject);
	_DiffColorR->eventEditTextChange = MyGUI::newDelegate(this, &MeshEditViewPage::UpdateEditedObject);
	_DiffColorB->eventEditTextChange = MyGUI::newDelegate(this, &MeshEditViewPage::UpdateEditedObject);
	_DiffColorG->eventEditTextChange = MyGUI::newDelegate(this, &MeshEditViewPage::UpdateEditedObject);
	_SpecColorR->eventEditTextChange = MyGUI::newDelegate(this, &MeshEditViewPage::UpdateEditedObject);
	_SpecColorG->eventEditTextChange = MyGUI::newDelegate(this, &MeshEditViewPage::UpdateEditedObject);
	_SpecColorB->eventEditTextChange = MyGUI::newDelegate(this, &MeshEditViewPage::UpdateEditedObject);
	_SpotMid->eventEditTextChange = MyGUI::newDelegate(this, &MeshEditViewPage::UpdateEditedObject);
	_SpotOutter->eventEditTextChange = MyGUI::newDelegate(this, &MeshEditViewPage::UpdateEditedObject);
	_ObjectName->eventEditSelectAccept = MyGUI::newDelegate(this, &MeshEditViewPage::UpdateEditedObject);
	_SpecColor->eventMouseLostFocus = MyGUI::newDelegate(this, &MeshEditViewPage::UpdateEditedObjectColor);
	_DiffColor->eventMouseLostFocus = MyGUI::newDelegate(this, &MeshEditViewPage::UpdateEditedObjectColor);
	_ColorPanelPage = ColorPanelViewPage::getInstancePtr();

	PageName = "EditObjectPageView";
}

void MeshEditViewPage::UpdateEditedObjectColor(MyGUI::Widget* _sender, MyGUI::Widget* _new)
{
	UpdateEditedObject(NULL);
}

MeshEditViewPage::~MeshEditViewPage()
{
}

void MeshEditViewPage::ShowGuiWindow()
{
	_Window->setVisible(true);
	_isVisible = true;
}
void MeshEditViewPage::HideGuiWindow()
{
	_Window->setVisible(false);
	_isVisible = false;
}

void MeshEditViewPage::Update()
{

}

void MeshEditViewPage::updateMeshPosition(Ogre::Vector3 position)
{
	_PositionX->setCaption(Ogre::StringConverter::toString(position.x));
	_PositionY->setCaption(Ogre::StringConverter::toString(position.y));
	_PositionZ->setCaption(Ogre::StringConverter::toString(position.z));

}
void MeshEditViewPage::updateMeshOrientation(Ogre::Quaternion orientation)
{
	_OrientationZ->setCaption(Ogre::StringConverter::toString(orientation.getRoll().valueDegrees()));
	_OrientationX->setCaption(Ogre::StringConverter::toString(orientation.getPitch().valueDegrees()));
	_OrientationY->setCaption(Ogre::StringConverter::toString(orientation.getYaw().valueDegrees()));	
}
void MeshEditViewPage::updateMeshScale(Ogre::Vector3 scale)
{
	_ScaleX->setCaption(Ogre::StringConverter::toString(scale.x));
	_ScaleY->setCaption(Ogre::StringConverter::toString(scale.y));
	_ScaleZ->setCaption(Ogre::StringConverter::toString(scale.z));
}
Ogre::Vector3 MeshEditViewPage::getMeshPosition()
{
	Ogre::Vector3 position;
	position.x = Ogre::StringConverter::parseReal(_PositionX->getCaption());
	position.y = Ogre::StringConverter::parseReal(_PositionY->getCaption());
	position.z = Ogre::StringConverter::parseReal(_PositionZ->getCaption());
	if (!position.isNaN()){
		return position;
	}
	else{
		return Ogre::Vector3::ZERO;
	}
}
Ogre::Quaternion MeshEditViewPage::getMeshOrientation()
{
	Ogre::Quaternion orientation;
	orientation = orientation * Ogre::Quaternion(Ogre::Radian(Ogre::Angle(Ogre::StringConverter::parseReal(_OrientationX->getCaption()))), Ogre::Vector3::UNIT_X);
	orientation = orientation * Ogre::Quaternion(Ogre::Radian(Ogre::Angle(Ogre::StringConverter::parseReal(_OrientationY->getCaption()))), Ogre::Vector3::UNIT_Y);
	orientation = orientation * Ogre::Quaternion(Ogre::Radian(Ogre::Angle(Ogre::StringConverter::parseReal(_OrientationZ->getCaption()))), Ogre::Vector3::UNIT_Z);
	if (!orientation.isNaN()){
		return orientation;
	}
	else{
		return Ogre::Quaternion::IDENTITY;
	}

}
Ogre::Vector3 MeshEditViewPage::getMeshScale()
{
	Ogre::Vector3 scale;
	scale.x = Ogre::StringConverter::parseReal(_ScaleX->getCaption());
	scale.y = Ogre::StringConverter::parseReal(_ScaleY->getCaption());
	scale.z = Ogre::StringConverter::parseReal(_ScaleZ->getCaption());
	if (!scale.isNaN()){
		return scale;
	}
	else{
		return Ogre::Vector3::ZERO;
	}
}

void MeshEditViewPage::UpdateEditedObject(MyGUI::EditBox* _sender)
{
	//_sender->setCaption(deleteNoNNumberCharacter(_sender->getCaption()));
	ObjectsMask type = getType(_ObjectType->getCaption());
	MeshDate date;
	date.name = _ObjectName->getCaption();
	date.position = getMeshPosition();
	if (type == EntityMask){
		date.scale = getMeshScale();
	}
	if (type == EntityMask || type == LightsMasks){
		date.orientatnion = getMeshOrientation();
	}
	if (type == LightsMasks){
		date.diffuseColor = changeMyGUItoOgreColour(getDiffColour());
		date.specularColor = changeMyGUItoOgreColour(getSpecColour());
		date.spotlight.x = StringConverter::parseReal(_SpotMid->getCaption());
		date.spotlight.y = StringConverter::parseReal(_SpotOutter->getCaption());
	}
	EditedObject::getSingleton().updateObjectDate(date);
}

void MeshEditViewPage::UpdateMeshViewDate(MeshDate date)
{
	_EditPosition->setVisible(true);
	updateMeshPosition(date.position);
	if (date.type == EntityMask){
		_EditScale->setVisible(true);
		updateMeshScale(date.scale);
	}
	else{
		_EditScale->setVisible(false);
	}
	if (date.type == EntityMask || date.type == LightsMasks ){
		_EditOrientation->setVisible(true);
		updateMeshOrientation(date.orientatnion);
	}
	else{
		_EditOrientation->setVisible(false);
	}
	if (date.type == LightsMasks){
		_EditLightColor->setVisible(true);
		setDiffColour(date.diffuseColor);
		setSpecColour(date.specularColor);
		//updateLightColors
		if (date.meshName.compare("LT_SPOTLIGHT") == 0){
			_SpotMid->setCaption(StringConverter::toString(date.spotlight.x));
			_SpotOutter->setCaption(StringConverter::toString(date.spotlight.y));
			_EditSpotlightRange->setVisible(true);
		}
		else{
			_EditSpotlightRange->setVisible(false);
		}
	}
	else{
		_EditLightColor->setVisible(false);
		_EditSpotlightRange->setVisible(false);
	}
	_ObjectName->setCaption(date.name);
	_ObjectType->setCaption(getTypeName(date.type));
}
MeshDate MeshEditViewPage::GetMeshDateFromView()
{
	ObjectsMask type = getType(_ObjectType->getCaption());
	MeshDate date;
	date.type = type;
	date.name = _ObjectName->getCaption();
	date.position = getMeshPosition();
	if (type == EntityMask){
		date.scale = getMeshScale();
	}
	if (type == EntityMask || type == LightsMasks){
		date.orientatnion = getMeshOrientation();
	}
	if (type == LightsMasks){
		date.diffuseColor = changeMyGUItoOgreColour(getDiffColour());
		date.specularColor = changeMyGUItoOgreColour(getSpecColour());
		date.spotlight.x = StringConverter::parseReal(_SpotMid->getCaption());
		date.spotlight.y = StringConverter::parseReal(_SpotOutter->getCaption());
	}
	return date;
}

std::string MeshEditViewPage::deleteNoNNumberCharacter(std::string string)
{
	auto iterator = string.begin();
	while (iterator != string.end())
	{
		if ((*iterator) != '.' && (*iterator) != ',' && (*iterator) != '-'){
			if ((*iterator)< '0' || (*iterator)>'9'){
				iterator = string.erase(iterator);
				continue;
			}
		}
		iterator++;
	}
	return string;
}

std::string MeshEditViewPage::getTypeName(ObjectsMask mask)
{
	if (mask == EntityMask)
		return "Entity";
	else if (mask == LightsMasks)
		return "Light";
	else if (mask == ParticlesMasks)
		return "Particle";
	else
		return "Unknown";
}

ObjectsMask MeshEditViewPage::getType(std::string typeName)
{
	if (typeName.compare("Entity") == 0)
		return EntityMask;
	if (typeName.compare("Light") == 0)
		return LightsMasks;
	if (typeName.compare("Particle") == 0)
		return ParticlesMasks;
	else
		return EntityMask;
}

MyGUI::Colour MeshEditViewPage::getDiffColour()
{
	float x = 1.f / 255.f;
	MyGUI::Colour diffColor;
	diffColor.red = x*StringConverter::parseReal(_DiffColorR->getCaption());
	diffColor.green = x*StringConverter::parseReal(_DiffColorG->getCaption());
	diffColor.blue = x*StringConverter::parseReal(_DiffColorB->getCaption());
	return diffColor;
}
MyGUI::Colour MeshEditViewPage::getSpecColour()
{
	float x = 1.f / 255.f;
	MyGUI::Colour specColor;
	specColor.red = x*StringConverter::parseReal(_SpecColorR->getCaption());
	specColor.green = x*StringConverter::parseReal(_SpecColorG->getCaption());
	specColor.blue = x*StringConverter::parseReal(_SpecColorB->getCaption());
	return specColor;
}
Ogre::ColourValue MeshEditViewPage::changeMyGUItoOgreColour(MyGUI::Colour colour)
{
	return Ogre::ColourValue(colour.red, colour.green, colour.blue);
}

MyGUI::Colour MeshEditViewPage::changeOgreToMyGuiColour(Ogre::ColourValue colour)
{
	return MyGUI::Colour(colour.r, colour.g, colour.b, colour.a);
}

void MeshEditViewPage::DiffColorButtonPressed(MyGUI::Widget* _sender)
{
	_ColorPanelPage->setColour(getDiffColour());
	_ColorPanelPage->setChangedWidgets(_DiffColorR, _DiffColorG, _DiffColorB, _DiffColor);
	_ColorPanelPage->ShowGuiWindow();
}
void MeshEditViewPage::SpecColorButtonPressed(MyGUI::Widget* _sender)
{
	_ColorPanelPage->setColour(getSpecColour());
	_ColorPanelPage->setChangedWidgets(_SpecColorR, _SpecColorG, _SpecColorB, _SpecColor);
	_ColorPanelPage->ShowGuiWindow();
}

void MeshEditViewPage::setDiffColour(Ogre::ColourValue colour)
{
	_DiffColor->setColour(changeOgreToMyGuiColour(colour));
	_DiffColorR->setCaption(StringConverter::toString(int(colour.r * 255)));
	_DiffColorG->setCaption(StringConverter::toString(int(colour.g * 255)));
	_DiffColorB->setCaption(StringConverter::toString(int(colour.b * 255)));
}
void MeshEditViewPage::setSpecColour(Ogre::ColourValue colour)
{
	_SpecColor->setColour(changeOgreToMyGuiColour(colour));
	_SpecColorR->setCaption(StringConverter::toString(int(colour.r * 255)));
	_SpecColorG->setCaption(StringConverter::toString(int(colour.g * 255)));
	_SpecColorB->setCaption(StringConverter::toString(int(colour.b * 255)));
}