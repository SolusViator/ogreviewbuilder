#pragma once
#include "GuiPage.h"
#include "MYGUI\MyGUI.h"
#include "Ogre.h"

class ColorPanelViewPage : public GuiPage
{
public:
	virtual void ShowGuiWindow();
	virtual void HideGuiWindow();
	void setChangedWidgets(MyGUI::EditBox*, MyGUI::EditBox*, MyGUI::EditBox*, MyGUI::Widget*);
	void setColour(const MyGUI::Colour& _colour);
	static ColorPanelViewPage* getInstancePtr();
private:
	static ColorPanelViewPage* _instance;

	void notifyMouseDrag(MyGUI::Widget* _sender, int _left, int _top, MyGUI::MouseButton _id);
	void notifyMouseButtonPressed(MyGUI::Widget* _sender, int _left, int _top, MyGUI::MouseButton _id);
	void notifyScrollChangePosition(MyGUI::ScrollBar* _sender, size_t _position);
	void notifyEditTextChange(MyGUI::EditBox* _sender);
	void notifyMouseButtonClick(MyGUI::Widget* _sender);

	void updateFirst();

	void createTexture();
	void updateTexture(const MyGUI::Colour& _colour);
	void destroyTexture();

	void updateFromPoint(const MyGUI::IntPoint& _point);
	void updateFromColour(const MyGUI::Colour& _colour);

	MyGUI::Colour getSaturate(const MyGUI::Colour& _colour) const;

	float& byIndex(MyGUI::Colour& _colour, size_t _index);

	void initialiseByAttributes();
private:
	MyGUI::ImageBox* mColourRect;
	MyGUI::Widget* mColourView;
	MyGUI::ImageBox* mImageColourPicker;
	MyGUI::EditBox* mEditRed;
	MyGUI::EditBox* mEditGreen;
	MyGUI::EditBox* mEditBlue;
	MyGUI::ScrollBar* mScrollRange;
	MyGUI::Button* mOk;
	MyGUI::Window* mWindow;

	MyGUI::Colour mCurrentColour;
	MyGUI::Colour mBaseColour;

	std::vector<MyGUI::Colour> mColourRange;

	MyGUI::ITexture* mTexture;

	MyGUI::EditBox* _destEditRed;
	MyGUI::EditBox* _destEditGreen;
	MyGUI::EditBox* _destEditBlue;
	MyGUI::Widget* _actualColor;

	ColorPanelViewPage();
	~ColorPanelViewPage();
};

