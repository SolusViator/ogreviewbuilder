#pragma once

class MaskController{
public:
	MaskController(){
		_mask = 0;
	}
	~MaskController(){}
	void setMaskBit(unsigned int setMaskBit){ _mask |= setMaskBit; }
	void removeMaskBit(unsigned int removeMaskBit){ _mask &= ~(removeMaskBit); }
	bool checkMaskBit(unsigned int checkMaskBit){ return _mask&checkMaskBit; }	
protected:
	void setMaskBit(unsigned int mask, unsigned int setMaskBit){ mask |= setMaskBit; }
	void removeMaskBit(unsigned int mask, unsigned int removeMaskBit){ mask &= ~(removeMaskBit); }
	bool checkMaskBit(unsigned int mask, unsigned int checkMaskBit){ return mask&checkMaskBit; }
	unsigned int _mask;
};