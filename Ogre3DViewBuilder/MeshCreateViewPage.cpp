#include "MeshCreateViewPage.h"


MeshCreateViewPage::MeshCreateViewPage(ResourcesList* resList, GuiController* guiController)
{
	MyGUI::Gui* gui = MyGUI::Gui::getInstancePtr();
	MyGUI::LayoutManager::getInstancePtr()->loadLayout("MeshCreateMenu.layout");
	_Window = gui->findWidget<MyGUI::Window>("_CreateWindow");
	_PositionX = gui->findWidget<MyGUI::EditBox>("MeshPositionX");
	_PositionY = gui->findWidget<MyGUI::EditBox>("MeshPositionY");	
	_PositionZ = gui->findWidget<MyGUI::EditBox>("MeshPositionZ");
	_RotationX = gui->findWidget<MyGUI::EditBox>("MeshRotationX");
	_RotationY = gui->findWidget<MyGUI::EditBox>("MeshRotationY");
	_RotationZ = gui->findWidget<MyGUI::EditBox>("MeshRotationZ");
	_RotationW = gui->findWidget<MyGUI::EditBox>("MeshRotationW");
	_ScaleX = gui->findWidget<MyGUI::EditBox>("MeshScaleX");
	_ScaleY = gui->findWidget<MyGUI::EditBox>("MeshScaleY");
	_ScaleZ = gui->findWidget<MyGUI::EditBox>("MeshScaleZ");

	_meshesComboBox = gui->findWidget<MyGUI::ComboBox>("Meshes");
	gui->findWidget<MyGUI::Button>("CreateMeshButton")->eventMouseButtonClick = 
		MyGUI::newDelegate(guiController, &GuiController::CreateMeshButtonClicked);
	gui->findWidget<MyGUI::Button>("SetMeshCursorPosition")->eventMouseButtonClick =
		MyGUI::newDelegate(this, &MeshCreateViewPage::setCursorPosition);
	gui->findWidget<MyGUI::Button>("ParseScene")->eventMouseButtonClick = 
		MyGUI::newDelegate(this, &MeshCreateViewPage::parseScene);
	PageName = "MeshCreateViewPage";
	loadMeshes(resList);
	ShowGuiWindow();
}


MeshCreateViewPage::~MeshCreateViewPage()
{
}

void MeshCreateViewPage::loadMeshes(ResourcesList* resList)
{
	auto iterator = resList->meshes.begin();
	while (iterator != resList->meshes.end()){
		_meshesComboBox->addItem((*iterator));
		iterator++;
	}
}

void MeshCreateViewPage::ShowGuiWindow()
{
	_Window->setVisible(true);
	_isVisible = true;
}
void MeshCreateViewPage::HideGuiWindow()
{
	_Window->setVisible(false);
	_isVisible = false;
}

MeshDate MeshCreateViewPage::getMeshDate()
{
	Ogre::Vector3 position;
	Ogre::Quaternion orientation;
	Ogre::Vector3 scale;
	Ogre::String name;
	position.x = Ogre::StringConverter::parseReal(_PositionX->getCaption());
	position.y = Ogre::StringConverter::parseReal(_PositionY->getCaption());
	position.z = Ogre::StringConverter::parseReal(_PositionZ->getCaption());

	scale.x = Ogre::StringConverter::parseReal(_ScaleX->getCaption());
	scale.y = Ogre::StringConverter::parseReal(_ScaleY->getCaption());
	scale.z = Ogre::StringConverter::parseReal(_ScaleZ->getCaption());
	
	orientation.x = Ogre::StringConverter::parseReal(_RotationX->getCaption());
	orientation.y = Ogre::StringConverter::parseReal(_RotationY->getCaption());
	orientation.z = Ogre::StringConverter::parseReal(_RotationZ->getCaption());
	orientation.w = Ogre::StringConverter::parseReal(_RotationW->getCaption());
	if (orientation.x || orientation.y || orientation.z == 0)
	{
		orientation.w = 1.0;
	}
	size_t index = _meshesComboBox->getIndexSelected();
	if (index != MyGUI::ITEM_NONE){
		name = _meshesComboBox->getCaption();
	}
	else{
		name.clear();
	}
	MeshDate date;
	date.meshName = name;
	date.orientatnion = orientation;
	date.position = position;
	date.scale = scale;
	date.type = EntityMask;
	return date;
}

void MeshCreateViewPage::setCursorPosition(MyGUI::Widget* _widget)
{
	Ogre::Vector3 pointerPosition = PointerController::getPointerPosition();
	_PositionX->setCaption(Ogre::StringConverter::toString(pointerPosition.x));
	_PositionY->setCaption(Ogre::StringConverter::toString(pointerPosition.y));
	_PositionZ->setCaption(Ogre::StringConverter::toString(pointerPosition.z));
}

void MeshCreateViewPage::parseScene(MyGUI::Widget* _widget)
{
	SceneManager::getSingleton().createSceneFile(MyGUI::Gui::getInstance().findWidget<MyGUI::EditBox>("SceneFileName")->getCaption());
}