#include "MeshManager.h"


MeshManager::MeshManager(Ogre::SceneManager *manager)
{
	_mSceneManager = manager;
	_entityNumber = 0;
}


MeshManager::~MeshManager(void)
{
}


Ogre::SceneNode* MeshManager::createEntityWithoutNode(Ogre::String meshName, Ogre::Vector3 position, Ogre::Quaternion orientation, Ogre::Vector3 scale)
{
	return createEntityToNode(NULL, meshName, position, orientation, scale);
}

Ogre::SceneNode* MeshManager::createEntityToNode(Ogre::SceneNode* parentNode, Ogre::String meshName,
	Ogre::Vector3 position, Ogre::Quaternion orientation, Ogre::Vector3 scale)
{
	if (!meshName.empty()){
		try{
			Ogre::SceneNode *node = NULL;
			Ogre::String tmpName = "Entity" + Ogre::StringConverter::toString(_entityNumber++);
			Ogre::Entity* entity = _mSceneManager->createEntity(tmpName, meshName);
			entity->setRenderQueueGroup(Ogre::RENDER_QUEUE_1);
			entity->setQueryFlags(EntityMask);
			if (parentNode){
				node = parentNode->createChildSceneNode(tmpName);
			}
			else{
				node = _mSceneManager->getRootSceneNode()->createChildSceneNode(tmpName);
			}
			node->attachObject(entity);
			node->setPosition(position);
			node->setScale(scale);
			if (!orientation.isNaN()){
				node->setOrientation(orientation);
			}
			return node;
		}
		catch (...){
			return NULL;
		}
	}
	return NULL;
}

Ogre::SceneNode* MeshManager::createParticleSystem(Ogre::String particleName,
	Ogre::Vector3 position,
	Ogre::Vector3 scale )
{
	if (!particleName.empty())
	{
		try{
			Ogre::String tmpName = "ParticleSystem" + Ogre::StringConverter::toString(_entityNumber++);
			Ogre::SceneNode* node = _mSceneManager->getRootSceneNode()->createChildSceneNode(tmpName);
			Ogre::ParticleSystem* particle = _mSceneManager->createParticleSystem(tmpName, particleName);
			particle->setBoundsAutoUpdated(false);
			Ogre::Entity* particleMesh = _mSceneManager->createEntity(tmpName+"Mesh", "Particle.mesh");
			particleMesh->setQueryFlags(ParticlesMasks);
			node->setPosition(position);
			node->setScale(scale);
			node->attachObject(particleMesh);
			node->attachObject(particle);
			return node;
		}
		catch (...){
			return NULL;
		}
	}
	return NULL;
}

Ogre::SceneNode* MeshManager::createSceneNode(Ogre::String name)
{
	auto node = _mSceneManager->getRootSceneNode()->createChildSceneNode(name + Ogre::StringConverter::toString(_entityNumber++));
	return node;
}

Ogre::SceneNode* MeshManager::findSceneNode(Ogre::String name)
{
	try{
		return _mSceneManager->getSceneNode(name);
	}
	catch (...){
		return NULL;
	}
}

Ogre::Entity* MeshManager::createEntity(Ogre::String name)
{
	auto tmpname = name + Ogre::StringConverter::toString(_entityNumber++);
	auto ent = _mSceneManager->createEntity(tmpname, name);
	ent->getSubEntity(0)->getMaterial()->setDepthCheckEnabled(false);
	ent->getSubEntity(0)->getMaterial()->setDepthWriteEnabled(false);
	return ent;
}

Ogre::SceneNode* MeshManager::createLight(Ogre::String LightType, Ogre::Vector3 position, Ogre::ColourValue specColor, Ogre::ColourValue diffColor, Ogre::Vector2 spotlight)
{
	if (!LightType.empty()){
		Ogre::Light* light = NULL;
		auto tmpName = LightType + Ogre::StringConverter::toString(_entityNumber++);
		if (LightType.compare("LT_POINT")){
			light = _mSceneManager->createLight(tmpName);
			light->setType(Ogre::Light::LT_POINT);
			light->setAttenuation(100, 1.0, 0.045, 0.0075);
		}
		else if (LightType.compare("LT_SPOTLIGHT")){
			light = _mSceneManager->createLight(tmpName);
			light->setType(Ogre::Light::LT_SPOTLIGHT);
			light->setSpotlightRange(Ogre::Radian(Ogre::Degree(spotlight.x)), Ogre::Radian(Ogre::Degree(spotlight.y)));
		}
		else if (LightType.compare("LT_DIRECTIONAL")){
			light = _mSceneManager->createLight(tmpName);
			light->setType(Ogre::Light::LT_DIRECTIONAL);
		}
		else{
			return NULL;
		}
		light->setSpecularColour(specColor); 
		light->setDiffuseColour(diffColor);
		light->setDirection(Ogre::Vector3(0, -1, 0));
		auto node = _mSceneManager->getRootSceneNode()->createChildSceneNode(tmpName);
		node->setPosition(position);
		auto lightMesh = _mSceneManager->createEntity(tmpName + "Mesh", "Particle.mesh");
		lightMesh->setQueryFlags(ParticlesMasks);
		node->attachObject(lightMesh);
		node->attachObject(light);
		return node;
	}
	return NULL;
}