#include "Pointer.h"


Pointer::Pointer(Ogre::SceneNode* pointerNode)
{
	if (pointerNode){
		_pointerNode = pointerNode;
		_pointerNode->getAttachedObject(0)->setQueryFlags(0);
	}
}


Pointer::~Pointer()
{
}

Ogre::Vector3 Pointer::getPointerPosition()
{
	return _pointerNode->getPosition();
}
void Pointer::setPointerPosition(Ogre::Vector3 newPosition)
{
	_pointerNode->setPosition(newPosition);
}