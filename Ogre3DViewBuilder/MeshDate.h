#pragma once
#include "OgreVector3.h"
#include "OgreQuaternion.h"
#include "OgreString.h"
#include "OgreSceneNode.h"
#include "ObjectsMask.h"
#include "OgreColourValue.h"
struct MeshDate
{
	Ogre::Vector3 position;
	Ogre::Vector3 scale;
	Ogre::Quaternion orientatnion;
	Ogre::String name;
	Ogre::String meshName;
	Ogre::SceneNode* ObjectNode;
	ObjectsMask type;
	Ogre::ColourValue specularColor;
	Ogre::ColourValue diffuseColor;
	Ogre::Vector2 spotlight;
};