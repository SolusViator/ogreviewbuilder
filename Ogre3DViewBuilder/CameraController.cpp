#include "CameraController.h"


CameraController::CameraController(Camera* camera)
{
	if (camera != NULL) _mCamera = camera;
	_MovementMask = 0;
	_MouseMask = 0;
}


CameraController::~CameraController()
{
}

void CameraController::ResetPosition()
{
	//_mCamera->setCameraOrientation();
}

Ogre::Quaternion CameraController::getCameraOrientatnion()
{
	return _mCamera->getCameraOrientation();
}

void CameraController::MoveCameraForward(float distance)
{
	_mCamera->setCameraRelPosition(_mCamera->getCameraDirection()*distance); // przemnożyć przez czas
}
void CameraController::MoveCameraBackward(float distance)
{
	MoveCameraForward(-distance);
}
void CameraController::MoveCameraLeft(float distance)
{
	MoveCameraRight(-distance);
}
void CameraController::MoveCameraRight(float distance)
{
	_mCamera->setCameraRelPosition(_mCamera->getCameraOrientation()*Ogre::Vector3(1, 0, 0)*distance); // TO DO
}

void CameraController::RotateCamera(float angle, Ogre::Vector3 axes)
{
	_mCamera->setCameraOrientation(_mCamera->getCameraOrientation()*Ogre::Quaternion(Ogre::Radian(Ogre::Degree(angle)), axes));
	//if (axes.x){
	//	_mCamera->setCameraRoll(Ogre::Radian(Ogre::Degree(angle)));
	//}
	//if (axes.y){
	//	_mCamera->setCameraYaw(Ogre::Radian(Ogre::Degree(angle)));
	//}
	//if (axes.z){
	//	_mCamera->setCameraPitch(Ogre::Radian(Ogre::Degree(angle)));
	//}
}

void CameraController::MoveCamera(Ogre::Vector3 direction, float distance)
{
	_mCamera->setCameraRelPosition(direction*distance);
}


void CameraController::setKeyboardMaskBit(unsigned int state)
{
	_MovementMask |= state;
}
void CameraController::removeKeyboardMaskBit(unsigned int state)
{
	_MovementMask &= ~(state);
}

void CameraController::setMouseMaskBit(unsigned int state)
{
	_MouseMask |= state;
}
void CameraController::removeMouseMaskBit(unsigned int state)
{
	_MouseMask &= ~(state);
}

void CameraController::UpdateCamera()
{
	//ResetPosition();

	if (!(_MouseMask&releaseMouse)){
		if (_MovementMask&moveForward){
			MoveCameraForward(1.f);
		}
		else if (_MovementMask&moveBackward){
			MoveCameraBackward(1.f);
		}
		if (_MovementMask&moveLeft){
			MoveCameraLeft(1.0f);
		}
		else if (_MovementMask&moveRight){
			MoveCameraRight(1.0f);
		}
		if (_MouseMask&moveUp)
		{
			MoveCamera(Ogre::Vector3(0, -1, 0), 0.5);
		}
		else if (_MouseMask&moveDown)
		{
			MoveCamera(Ogre::Vector3(0, 1, 0), 0.5);
		}
		if (_mouseXDistance < _mouseYDistance){
			if (_MouseMask&rotateDown){
				RotateCamera(_mouseYDistance, Ogre::Vector3(1, 0, 0));
			}
			else if (_MouseMask&rotateUp){
				RotateCamera(-_mouseYDistance, Ogre::Vector3(1, 0, 0));
			}
		}
		else{
			if (_MouseMask&rotateLeft){
				RotateCamera(_mouseXDistance, Ogre::Vector3(0, 1, 0));
			}
			else if (_MouseMask&rotateRight){
				RotateCamera(-_mouseXDistance, Ogre::Vector3(0, 1, 0));
			}
		}
	}
	removeMouseMaskBit(~(unsigned int)0);
}

Ogre::Ray CameraController::getCameraToViewportRay(float positionX, float positionY)
{
	return _mCamera->getCameraToViewportRay(positionX, positionY);
}

Ogre::Vector3 CameraController::getCameraPosition()
{
	return _mCamera->getCameraPosition();
}
Ogre::Vector3 CameraController::getCameraDirection()
{
	return _mCamera->getCameraDirection();
}

void CameraController::setMouseRelDistance(Ogre::Vector2 distance)
{
	_mouseXDistance = abs(distance.x*0.1);
	_mouseYDistance = abs(distance.y*0.1);
}