#pragma once
#include "Pointer.h"


class PointerController 
{
public:
	PointerController(Pointer* pointer);
	PointerController(Ogre::SceneNode* pointerNode);
	~PointerController();
	static Ogre::Vector3 getPointerPosition();
	void setPointerPosition(Ogre::Vector3 newPostion);
protected:


	static Pointer* _Pointer;
};

