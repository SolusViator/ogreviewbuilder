#include "SceneManager.h"

SceneManager* SceneManager::_instance = NULL;

SceneManager::SceneManager()
{
	_sceneTree.clear();
}

SceneManager::~SceneManager()
{
}

SceneManager& SceneManager::getSingleton()
{
	if (_instance == NULL){
		_instance = new SceneManager();
	}
	return *_instance;
}

void SceneManager::addNewObject(SceneObject* object)
{
	if (object != NULL && object->validate()){
		object->setNewParent(NULL);
		this->_sceneTree.push_back(object);
	}
}

SceneObject* SceneManager::findObject(Ogre::SceneNode* node)
{
	SceneObject* tmp = NULL;
	auto iterator = this->_sceneTree.begin();
	for (; iterator != _sceneTree.end(); iterator++){
		if ((*iterator)->compareSceneNode(node)){
			tmp = (*iterator);
		}
		else{
			tmp = (*iterator)->findObject(node);
		}
		if (tmp != NULL){ return tmp; }
	}
	return NULL;
}
SceneObject* SceneManager::findObject(Ogre::String name)
{
	SceneObject* tmp = NULL;
	auto iterator = this->_sceneTree.begin();
	for (; iterator != _sceneTree.end(); iterator++){
		if ((*iterator)->getName().compare(name) == 0){
			tmp = (*iterator);
		}
		else{
			tmp = (*iterator)->findObject(name);
		}
		if (tmp != NULL){ return tmp; }
	}
	return NULL;
}

bool SceneManager::createSceneFile(Ogre::String fileName)
{
	try{
		TiXmlDocument sceneDocument;
		TiXmlDeclaration* declaration = new TiXmlDeclaration("1.0", "", "");
		sceneDocument.LinkEndChild(declaration);

		TiXmlElement * root = new TiXmlElement("scene");
		sceneDocument.LinkEndChild(root);

		TiXmlElement* nodes = new TiXmlElement("nodes");
		root->LinkEndChild(nodes);

		auto iterator = this->_sceneTree.begin();
		for (; iterator != _sceneTree.end(); iterator++){
			(*iterator)->CreateXmlSceneNode(nodes);
		}

		if (fileName.length() > 5)
			sceneDocument.SaveFile((fileName + ".scene").c_str());
		else
			sceneDocument.SaveFile("OutputFile.scene");
		return true;
	}
	catch (...){
		return false;
	}
}