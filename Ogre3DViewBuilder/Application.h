#pragma once
#include "Engine.h"
#include "ResourcesList.h"
class Application
{
public:
	void RunApplication();
	Application();
	~Application();
private:
	Engine* _Engine;
	ResourcesList* _ResourcesList;
};

