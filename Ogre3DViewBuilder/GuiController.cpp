#include "GuiController.h"


GuiController::GuiController()
{
	_Masks.insert(std::pair<MaskType, unsigned>(CreateObjectMask, (unsigned int)0));
	_Masks.insert(std::pair<MaskType, unsigned>(ChangePageViewMask, (unsigned int)0));

}


GuiController::~GuiController()
{
}

void GuiController::setMask(MaskType mask, unsigned int maskValue)
{
	auto it = _Masks.find(mask);
	if (it != _Masks.end()){
		it->second |= maskValue;
	}
}
unsigned int GuiController::getMask(MaskType mask)
{
	auto it = _Masks.find(mask);
	if (it != _Masks.end()){
		return it->second;
	}
	return 0;
}

void GuiController::removeMask(MaskType mask, unsigned int maskValue)
{
	auto it = _Masks.find(mask);
	if (it != _Masks.end()){
		it->second &= ~(maskValue);
	}
}

void GuiController::CreateMeshButtonClicked(MyGUI::Widget* _widget)
{
	setMask(CreateObjectMask, Mesh);
}

void GuiController::CreateParticleButtonClicked(MyGUI::Widget* _widget)
{
	setMask(CreateObjectMask, ParticleEffect);
}

void GuiController::CreateLightButtonClicked(MyGUI::Widget* _widget)
{
	setMask(CreateObjectMask, Light);
}