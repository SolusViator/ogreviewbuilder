#pragma once
#include "Ogre.h"
class Camera
{
public:
	Camera(Ogre::SceneManager* mSceneManager, Ogre::RenderWindow* mWindow);
	bool CreateCamera(Ogre::String name = "DefaultCamera", Ogre::ColourValue backgroundColor = Ogre::ColourValue(0.f,0.f,0.f), float nearDistance = 1, float farDistance = 0, Ogre::Real aspectRatio = 0);  //TODO
	void enableCompositor(Ogre::String comName);
	void disableCompositor(Ogre::String comName);
	Ogre::Vector3 getCameraDirection();
	void setCameraDirection(Ogre::Vector3 direction);
	void setCameraRelDirection(Ogre::Vector3 direction);
	void setCameraPosition(Ogre::Vector3 position);
	void setCameraRelPosition(Ogre::Vector3 position);
	void setCameraRelRotation(Ogre::Quaternion angle);
	void setCameraOrientation(Ogre::Quaternion orientation);
	void setCameraPitch(Ogre::Radian rad);
	void setCameraYaw(Ogre::Radian rad);
	void setCameraRoll(Ogre::Radian rad);
	Ogre::Vector3 getCameraPosition();
	Ogre::Ray getCameraRay();
	Ogre::Ray getCameraToViewportRay(float screenX, float screenY);
	Ogre::Quaternion getCameraOrientation();
	void lookCameraAt(Ogre::Vector3 point);
	~Camera();
private:
	Ogre::SceneManager* _mSceneManager;
	Ogre::RenderWindow* _mWindow;
	Ogre::Camera* _mCamera;
	Ogre::SceneNode* _mCameraNode;
};

