#include "InputListener.h"


InputListener::InputListener(CameraController* controller, GuiController* guiController, EditController* editController )
{
	if (controller){
		_Controller = controller;
	}
	if (guiController){
		_GuiController = guiController;
	}
	if (editController)
	{
		_EditController = editController;
	}
	_SpecialKeysMask = 0;
}


InputListener::~InputListener()
{
}

bool InputListener::keyPressed(const OIS::KeyEvent& arg)
{
	if (_Controller){
		switch (arg.key){
		case OIS::KC_UP:
			_Controller->setKeyboardMaskBit(moveForward);
			break;
		case OIS::KC_DOWN:
			_Controller->setKeyboardMaskBit(moveBackward);
			break;
		case OIS::KC_LEFT:
			_Controller->setKeyboardMaskBit(moveLeft);
			break;
		case OIS::KC_RIGHT:
			_Controller->setKeyboardMaskBit(moveRight);
			break;
		case OIS::KC_LMENU:
			_Controller->setMouseMaskBit(releaseMouse);
			setMaskBit(Alt);
			break;
		case OIS::KC_R:
			if (_SpecialKeysMask&Ctrl){
				_Controller->setKeyboardMaskBit(resetPosition);
			}
			break;
		case OIS::KC_LSHIFT:
		case OIS::KC_RSHIFT:
			setMaskBit(Shift);
			break;
		case OIS::KC_RCONTROL:
		case OIS::KC_LCONTROL:
			setMaskBit(Ctrl);
			break;
		case OIS::KC_ESCAPE:
			if (_SpecialKeysMask&Ctrl)
				exit(0);
		}
	}
	MyGUI::InputManager::getInstance().injectKeyPress(MyGUI::KeyCode::Enum(arg.key), arg.text);
	return true;
}

bool InputListener::keyReleased(const OIS::KeyEvent& arg)
{
	if (_Controller && _GuiController){
		switch (arg.key){
		case OIS::KC_UP:
			_Controller->removeKeyboardMaskBit(moveForward);
			break;
		case OIS::KC_DOWN:
			_Controller->removeKeyboardMaskBit(moveBackward);
			break;
		case OIS::KC_LEFT:
			_Controller->removeKeyboardMaskBit(moveLeft);
			break;
		case OIS::KC_RIGHT:
			_Controller->removeKeyboardMaskBit(moveRight);
			break;
		case OIS::KC_C:
			if (_SpecialKeysMask&Ctrl);
			else{
				_GuiController->setMask(ChangePageViewMask, MeshCreatePage);
			}
			break;
		case OIS::KC_W:
			_EditController->setManipulatorType(Axes::Move);
			break;
		case OIS::KC_E:
			_EditController->setManipulatorType(Axes::Scale);
			break;
		case OIS::KC_R:
			_EditController->setManipulatorType(Axes::Rotate);
			break;
		case OIS::KC_LMENU:
			_Controller->removeMouseMaskBit(releaseMouse);
			removeMaskBit(Alt);
			break;
		case OIS::KC_LSHIFT:
		case OIS::KC_RSHIFT:
			removeMaskBit(Shift);
			break;
		case OIS::KC_RCONTROL:
		case OIS::KC_LCONTROL:
			removeMaskBit(Ctrl);
			break;
		}
	}


	MyGUI::InputManager::getInstance().injectKeyRelease(MyGUI::KeyCode::Enum(arg.key));
	return true;
}

bool InputListener::mouseMoved(const OIS::MouseEvent& arg)
{
	if (_Controller)
	{
		if (arg.state.X.rel > 0 && _SpecialKeysMask&Ctrl){

			_Controller->setMouseMaskBit(rotateRight);
		}
		else
			if (arg.state.X.rel < 0 && _SpecialKeysMask&Ctrl){
				_Controller->setMouseMaskBit(rotateLeft);
			}
		if (arg.state.Y.rel > 0 && _SpecialKeysMask&Ctrl){
			_Controller->setMouseMaskBit(rotateUp);
		}
		else
			if (arg.state.Y.rel < 0 && _SpecialKeysMask&Ctrl){
				_Controller->setMouseMaskBit(rotateDown);
			}
		if (arg.state.Y.rel > 0 && _SpecialKeysMask&Shift){
			_Controller->setMouseMaskBit(moveUp);
		}
		else
			if (arg.state.Y.rel < 0 && _SpecialKeysMask&Shift){
				_Controller->setMouseMaskBit(moveDown);
			}
		_Controller->setMouseRelDistance(Ogre::Vector2(arg.state.X.rel, arg.state.Y.rel));
		_EditController->setMouseRelPosition(Ogre::Vector2(arg.state.X.rel, arg.state.Y.rel));
	}
	MyGUI::InputManager::getInstance().injectMouseMove(arg.state.X.abs, arg.state.Y.abs, arg.state.Z.abs);
	return true;
}

bool InputListener::mousePressed(const OIS::MouseEvent& arg, OIS::MouseButtonID id)
{
	switch (id)

	{
	case OIS::MB_Left:
		auto ray = _Controller->getCameraToViewportRay(arg.state.X.abs, arg.state.Y.abs);
		auto res = getSceneQueryResult(ray, ArrowEditMask);
		_EditController->LeftButtonPressed(res);
		break;
	//case OIS::MB_Right:
	//	break;
	//case OIS::MB_Middle:
	//	break;
	//case OIS::MB_Button3:
	//	break;
	//case OIS::MB_Button4:
	//	break;
	//case OIS::MB_Button5:
	//	break;
	//case OIS::MB_Button6:
	//	break;
	//case OIS::MB_Button7:
	//	break;
	//default:
	//	break;
	}
	MyGUI::InputManager::getInstance().injectMousePress(arg.state.X.abs, arg.state.Y.abs, MyGUI::MouseButton::Enum(id));
	return true;
}

bool InputListener::mouseReleased(const OIS::MouseEvent& arg, OIS::MouseButtonID id)
{
	switch (id)
	{
		case OIS::MB_Left:
			_EditController->LeftButtonRelease();
			break;
		case OIS::MB_Right:
			auto hitPoint = Ogre::Vector3::ZERO;
			auto ray = _Controller->getCameraToViewportRay(arg.state.X.abs, arg.state.Y.abs);
			auto res = getSceneQueryResult(ray, EntityMask|ParticlesMasks, hitPoint );
		_EditController->ChangeEditingObject(res,ray, hitPoint);
		break;
		//case OIS::MB_Middle:
		//	break;
		//case OIS::MB_Button3:
		//	break;
		//case OIS::MB_Button4:
		//	break;
		//case OIS::MB_Button5:
		//	break;
		//case OIS::MB_Button6:
		//	break;
		//case OIS::MB_Button7:
		//	break;
	}
	MyGUI::InputManager::getInstance().injectMouseRelease(arg.state.X.abs, arg.state.Y.abs, MyGUI::MouseButton::Enum(id));
	return true;
}

void InputListener::setMaskBit(unsigned int state)
{
	_SpecialKeysMask |= state;
}
void InputListener::removeMaskBit(unsigned int state)
{
	_SpecialKeysMask &= ~(state);
}

Ogre::SceneNode* InputListener::getSceneQueryResult(Ogre::Ray ray, unsigned int mask, Ogre::Vector3& hitPoint)
{
	// create the ray scene query object
	auto m_pray_scene_query = mgr->createRayQuery(Ogre::Ray(), Ogre::SceneManager::WORLD_GEOMETRY_TYPE_MASK);
	if (NULL == m_pray_scene_query)
	{
		//LOG_ERROR << "Failed to create Ogre::RaySceneQuery instance" << ENDLOG;
		return NULL;
	}
	m_pray_scene_query->setSortByDistance(true);
	m_pray_scene_query->setQueryMask(mask);

	// check we are initialised
	if (m_pray_scene_query != NULL)
	{
		// create a query object
		m_pray_scene_query->setRay(ray);

		// execute the query, returns a vector of hits
		if (m_pray_scene_query->execute().size() <= 0)
		{
			// raycast did not hit an objects bounding box
			return (false);
		}
	}
	else
	{
		//LOG_ERROR << "Cannot raycast without RaySceneQuery instance" << ENDLOG;
		return NULL;
	}

	// at this point we have raycast to a series of different objects bounding boxes.
	// we need to test these different objects to see which is the first polygon hit.
	// there are some minor optimizations (distance based) that mean we wont have to
	// check all of the objects most of the time, but the worst case scenario is that
	// we need to test every triangle of every object.
	Ogre::SceneNode* closestSceneNode = NULL;
	Ogre::Real closest_distance = -1.0f;
	Ogre::Vector3 closest_result;
	Ogre::RaySceneQueryResult &query_result = m_pray_scene_query->getLastResults();
	for (size_t qr_idx = 0; qr_idx < query_result.size(); qr_idx++)
	{
		// stop checking if we have found a raycast hit that is closer
		// than all remaining entities
		if ((closest_distance >= 0.0f) &&
			(closest_distance < query_result[qr_idx].distance))
		{
			break;
		}

		// only check this result if its a hit against an entity
		if ((query_result[qr_idx].movable != NULL) &&
			(query_result[qr_idx].movable->getMovableType().compare("Entity") == 0))
		{
			// get the entity to check
			Ogre::Entity *pentity = static_cast<Ogre::Entity*>(query_result[qr_idx].movable);
			// mesh data to retrieve         
			size_t vertex_count;
			size_t index_count;
			Ogre::Vector3 *vertices;
			unsigned long *indices;

			// get the mesh information
			GetMeshInformation(pentity->getMesh(), vertex_count, vertices, index_count, indices,
				pentity->getParentNode()->_getDerivedPosition(),
				pentity->getParentNode()->_getDerivedOrientation(),
				pentity->getParentNode()->_getDerivedScale());

			// test for hitting individual triangles on the mesh
			bool new_closest_found = false;
			for (int i = 0; i < static_cast<int>(index_count); i += 3)
			{
				// check for a hit against this triangle
				std::pair<bool, Ogre::Real> hit = Ogre::Math::intersects(ray, vertices[indices[i]],
					vertices[indices[i + 1]], vertices[indices[i + 2]], true, false);

				// if it was a hit check if its the closest
				if (hit.first)
				{
					if ((closest_distance < 0.0f) ||
						(hit.second < closest_distance))
					{
						// this is the closest so far, save it off
						closest_distance = hit.second;
						closestSceneNode = pentity->getParentSceneNode();
						hitPoint = ray.getPoint(hit.second);
						new_closest_found = true;
					}
				}
			}

			// free the verticies and indicies memory
			delete[] vertices;
			delete[] indices;
		}
	}
	return closestSceneNode;
}

// Get the mesh information for the given mesh.
// Code found in Wiki: www.ogre3d.org/wiki/index.php/RetrieveVertexData
void InputListener::GetMeshInformation(const Ogre::MeshPtr mesh,
	size_t &vertex_count,
	Ogre::Vector3* &vertices,
	size_t &index_count,
	unsigned long* &indices,
	const Ogre::Vector3 &position,
	const Ogre::Quaternion &orient,
	const Ogre::Vector3 &scale)
{
	bool added_shared = false;
	size_t current_offset = 0;
	size_t shared_offset = 0;
	size_t next_offset = 0;
	size_t index_offset = 0;

	vertex_count = index_count = 0;

	// Calculate how many vertices and indices we're going to need
	for (unsigned short i = 0; i < mesh->getNumSubMeshes(); ++i)
	{
		Ogre::SubMesh* submesh = mesh->getSubMesh(i);

		// We only need to add the shared vertices once
		if (submesh->useSharedVertices)
		{
			if (!added_shared)
			{
				vertex_count += mesh->sharedVertexData->vertexCount;
				added_shared = true;
			}
		}
		else
		{
			vertex_count += submesh->vertexData->vertexCount;
		}

		// Add the indices
		index_count += submesh->indexData->indexCount;
	}


	// Allocate space for the vertices and indices
	vertices = new Ogre::Vector3[vertex_count];
	indices = new unsigned long[index_count];

	added_shared = false;

	// Run through the submeshes again, adding the data into the arrays
	for (unsigned short i = 0; i < mesh->getNumSubMeshes(); ++i)
	{
		Ogre::SubMesh* submesh = mesh->getSubMesh(i);

		Ogre::VertexData* vertex_data = submesh->useSharedVertices ? mesh->sharedVertexData : submesh->vertexData;

		if ((!submesh->useSharedVertices) || (submesh->useSharedVertices && !added_shared))
		{
			if (submesh->useSharedVertices)
			{
				added_shared = true;
				shared_offset = current_offset;
			}

			const Ogre::VertexElement* posElem =
				vertex_data->vertexDeclaration->findElementBySemantic(Ogre::VES_POSITION);

			Ogre::HardwareVertexBufferSharedPtr vbuf =
				vertex_data->vertexBufferBinding->getBuffer(posElem->getSource());

			unsigned char* vertex =
				static_cast<unsigned char*>(vbuf->lock(Ogre::HardwareBuffer::HBL_READ_ONLY));

			// There is _no_ baseVertexPointerToElement() which takes an Ogre::Real or a double
			//  as second argument. So make it float, to avoid trouble when Ogre::Real will
			//  be comiled/typedefed as double:
			//      Ogre::Real* pReal;
			float* pReal;

			for (size_t j = 0; j < vertex_data->vertexCount; ++j, vertex += vbuf->getVertexSize())
			{
				posElem->baseVertexPointerToElement(vertex, &pReal);

				Ogre::Vector3 pt(pReal[0], pReal[1], pReal[2]);

				vertices[current_offset + j] = (orient * (pt * scale)) + position;
			}

			vbuf->unlock();
			next_offset += vertex_data->vertexCount;
		}


		Ogre::IndexData* index_data = submesh->indexData;
		size_t numTris = index_data->indexCount / 3;
		Ogre::HardwareIndexBufferSharedPtr ibuf = index_data->indexBuffer;
		if (ibuf.isNull()) continue; // need to check if index buffer is valid (which will be not if the mesh doesn't have triangles like a pointcloud)

		bool use32bitindexes = (ibuf->getType() == Ogre::HardwareIndexBuffer::IT_32BIT);

		unsigned long*  pLong = static_cast<unsigned long*>(ibuf->lock(Ogre::HardwareBuffer::HBL_READ_ONLY));
		unsigned short* pShort = reinterpret_cast<unsigned short*>(pLong);


		size_t offset = (submesh->useSharedVertices) ? shared_offset : current_offset;
		size_t index_start = index_data->indexStart;
		size_t last_index = numTris * 3 + index_start;

		if (use32bitindexes)
			for (size_t k = index_start; k < last_index; ++k)
			{
				indices[index_offset++] = pLong[k] + static_cast<unsigned long>(offset);
			}

		else
			for (size_t k = index_start; k < last_index; ++k)
			{
				indices[index_offset++] = static_cast<unsigned long>(pShort[k]) +
					static_cast<unsigned long>(offset);
			}

		ibuf->unlock();
		current_offset = next_offset;
	}
}