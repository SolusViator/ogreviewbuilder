#include "ParticleCreateViewPage.h"


ParticleCreateViewPage::ParticleCreateViewPage(ResourcesList* resList, GuiController* guiController)
{
	MyGUI::Gui* gui = MyGUI::Gui::getInstancePtr();
	_Window = gui->findWidget<MyGUI::Window>("_CreateWindow");
	_PositionX = gui->findWidget<MyGUI::EditBox>("ParticlePositionX");
	_PositionY = gui->findWidget<MyGUI::EditBox>("ParticlePositionY");
	_PositionZ = gui->findWidget<MyGUI::EditBox>("ParticlePositionZ");

	_particlesComboBox = gui->findWidget<MyGUI::ComboBox>("Particles");
	gui->findWidget<MyGUI::Button>("CreateParticleButton")->eventMouseButtonClick =
		MyGUI::newDelegate(guiController, &GuiController::CreateParticleButtonClicked);
	gui->findWidget<MyGUI::Button>("ParticleCursorPosition")->eventMouseButtonClick =
		MyGUI::newDelegate(this, &ParticleCreateViewPage::setCursorPosition);
	PageName = "ParticleCreateViewPage";
	loadParticles(resList);
	ShowGuiWindow();
}


ParticleCreateViewPage::~ParticleCreateViewPage()
{
}

void ParticleCreateViewPage::loadParticles(ResourcesList* resList)
{
	auto iterator = resList->particles.begin();
	while (iterator != resList->particles.end()){
		_particlesComboBox->addItem((*iterator));
		iterator++;
	}
}

void ParticleCreateViewPage::ShowGuiWindow()
{
	_Window->setVisible(true);
	_isVisible = true;
}
void ParticleCreateViewPage::HideGuiWindow()
{
	_Window->setVisible(false);
	_isVisible = false;
}

MeshDate ParticleCreateViewPage::getParticleDate()
{
	Ogre::Vector3 position;
	Ogre::String name;
	position.x = Ogre::StringConverter::parseReal(_PositionX->getCaption());
	position.y = Ogre::StringConverter::parseReal(_PositionY->getCaption());
	position.z = Ogre::StringConverter::parseReal(_PositionZ->getCaption());

	size_t index = _particlesComboBox->getIndexSelected();
	if (index != MyGUI::ITEM_NONE){
		name = _particlesComboBox->getCaption();
	}
	else{
		name.clear();
	}
	MeshDate date;
	date.position = position;
	date.orientatnion = Ogre::Quaternion::IDENTITY;
	date.scale = Ogre::Vector3(1, 1, 1);
	date.meshName = name;
	date.type = ParticlesMasks;
	return date;
}

void ParticleCreateViewPage::setCursorPosition(MyGUI::Widget* _widget)
{
	Ogre::Vector3 pointerPosition = PointerController::getPointerPosition();
	_PositionX->setCaption(Ogre::StringConverter::toString(pointerPosition.x));
	_PositionY->setCaption(Ogre::StringConverter::toString(pointerPosition.y));
	_PositionZ->setCaption(Ogre::StringConverter::toString(pointerPosition.z));
}