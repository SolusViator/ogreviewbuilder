#include "PointerController.h"
#include "OgreExtension.h"
Pointer* PointerController::_Pointer = NULL;

PointerController::PointerController(Pointer* pointer)
{
	if (pointer){
		_Pointer = pointer;
	}
}

PointerController::PointerController(Ogre::SceneNode* pointerNode)
{
	_Pointer = new Pointer(pointerNode);
}

PointerController::~PointerController()
{
	_Pointer = NULL;
}

void PointerController::setPointerPosition(Ogre::Vector3 newPosition)
{
	_Pointer->setPointerPosition(newPosition);
}
Ogre::Vector3 PointerController::getPointerPosition()
{
	return _Pointer->getPointerPosition();
}
