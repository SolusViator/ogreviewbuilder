#include "EditedObject.h"

EditedObject* EditedObject::_editedObject = NULL;

EditedObject::EditedObject()
{
	this->_ObjectSceneNode = NULL;
}

EditedObject::EditedObject(EditedObject&)
{
}

EditedObject::~EditedObject()
{
	this->_ObjectSceneNode = NULL;
}

EditedObject& EditedObject::getSingleton()
{
	if (!_editedObject){
		_editedObject = new EditedObject();
	}
	return *_editedObject;
}

Ogre::Vector3 EditedObject::getObjectPosition()
{
	if (_ObjectSceneNode){
		return _ObjectSceneNode->getPosition();
	}
	else{
		return Ogre::Vector3::ZERO;
	}
}
Ogre::Quaternion EditedObject::getObjectOrientatnion()
{
	if (_ObjectSceneNode){
		return _ObjectSceneNode->getOrientation();
	}
	else{
		return Ogre::Quaternion::ZERO;
	}
}
Ogre::Vector3 EditedObject::getObjectScale()
{
	if (_ObjectSceneNode){
		return _ObjectSceneNode->getScale();
	}
	else{
		return Ogre::Vector3::ZERO;
	}
}

Ogre::Vector3 EditedObject::getObjectBoundingBoxSize()
{
	if (_ObjectSceneNode){
		return _ObjectSceneNode->getAttachedObjectIterator().getNext()->getBoundingBox().getSize();
	}
	else{
		return Ogre::Vector3::ZERO;
	}
}
Ogre::Vector3 EditedObject::getBoundingBoxCenterPosition()
{
	if (_ObjectSceneNode){
		return _ObjectSceneNode->getAttachedObjectIterator().getNext()->getBoundingBox().getCenter();
	}
	else{
		return Ogre::Vector3::ZERO;
	}
}

void EditedObject::setObjectPostion(Ogre::Vector3 newPosition)
{
	if (_ObjectSceneNode && !newPosition.isNaN())
	{
		_ObjectSceneNode->setPosition(newPosition);
	}
}
void EditedObject::setObjectOrientation(Ogre::Quaternion newOrientation)
{
	if (_ObjectSceneNode && !newOrientation.isNaN())
	{
		_ObjectSceneNode->setOrientation(newOrientation);
	}
}
void EditedObject::setObjectScale(Ogre::Vector3 newScale)
{
	if (_ObjectSceneNode && !newScale.isNaN())
	{
		_ObjectSceneNode->setScale(newScale);
	}
}

void EditedObject::setObject(SceneObject* object)
{
	if (this->_ObjectSceneNode){
		this->_ObjectSceneNode->showBoundingBox(false);
	}
	if (object != NULL){
		this->_ObjectSceneNode = object->getMeshDate().ObjectNode;
		this->_ObjectSceneNode->showBoundingBox(true);
		this->_object = object;
	}
}

void EditedObject::updateObjectDate(MeshDate newDate)
{
	if (SceneManager::getSingleton().findObject(newDate.name) != NULL){
		newDate.name.clear();
	}
	_object->setNewDate(newDate);
}

MeshDate EditedObject::getObjectDate()
{
	return _object->getMeshDate();
}

void EditedObject::updateObjectDate()
{
	_object->Update();
}