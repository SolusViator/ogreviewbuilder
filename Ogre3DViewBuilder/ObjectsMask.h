#pragma once
enum ObjectsMask
{
	EntityMask = 1,
	ParticlesMasks = 1 << 1,
	LightsMasks = 1 << 2,
	ArrowEditMask = 1 << 3
};