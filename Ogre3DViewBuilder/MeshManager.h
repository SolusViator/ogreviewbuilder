#pragma once
#include "Ogre.h"
#include "ObjectsMask.h"
class MeshManager
{

public:
	MeshManager(Ogre::SceneManager * manager);
	~MeshManager(void);
	Ogre::SceneNode* createEntityWithoutNode(Ogre::String meshName, 
										  Ogre::Vector3 position = Ogre::Vector3::ZERO,
										  Ogre::Quaternion orientation = Ogre::Quaternion::IDENTITY,
										  Ogre::Vector3 scale = Ogre::Vector3(1, 1, 1));
	Ogre::SceneNode* createEntityToNode(Ogre::SceneNode*, 
									 Ogre::String meshName, 
									 Ogre::Vector3 position = Ogre::Vector3::ZERO, 
									 Ogre::Quaternion orientation = Ogre::Quaternion::IDENTITY,
									 Ogre::Vector3 scale = Ogre::Vector3(1,1,1));
	Ogre::SceneNode* createParticleSystem(Ogre::String particleName,
		Ogre::Vector3 position = Ogre::Vector3::ZERO,
		Ogre::Vector3 scale = Ogre::Vector3(1, 1, 1));
	Ogre::Entity* createEntity(Ogre::String name);
	Ogre::SceneNode* createSceneNode(Ogre::String name);
	Ogre::SceneNode* findSceneNode(Ogre::String name);
	Ogre::SceneNode* createLight(Ogre::String LightType, Ogre::Vector3 position, Ogre::ColourValue specColor = Ogre::ColourValue::White, Ogre::ColourValue diffColor = Ogre::ColourValue::White, Ogre::Vector2 spotlight = Ogre::Vector2::ZERO);

protected:
	unsigned int _entityNumber;
	Ogre::SceneManager* _mSceneManager;
};

