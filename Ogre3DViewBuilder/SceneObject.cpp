#include "SceneObject.h"

using Ogre::StringConverter;

SceneObject::SceneObject(MeshDate date, SceneObject* parentObject)
{
	if (date.ObjectNode != NULL)
	{
		this->_SceneNode = date.ObjectNode;
	}
	this->_type = date.type;
	if (parentObject != NULL){
		this->_parent = parentObject;
	}
	else{
		_parent = NULL;
	}
	this->_date = date;
}


SceneObject::~SceneObject()
{
}

Ogre::String SceneObject::getName()
{
	return this->_date.name;
}
Ogre::String SceneObject::getMeshName()
{
	return this->_date.meshName;
}
SceneObject* SceneObject::findObject(Ogre::String Name) // OK
{
	SceneObject* tmp = NULL;
	if (this->_date.name.compare(Name) == 0){
		return this;
	}
	else{
		auto iterator = _childObjects.begin();
		for (; iterator != _childObjects.end(); iterator++){
			tmp = (*iterator)->findObject(Name);
			if (tmp != NULL){ return tmp; }
		}
	}
	return NULL;
}
SceneObject* SceneObject::findObject(Ogre::SceneNode* Node) // OK
{
	SceneObject* tmp = NULL;
	if (this->_SceneNode == Node){
		return this;
	}
	else{
		auto iterator = _childObjects.begin();
		for (; iterator != _childObjects.end(); iterator++){
			tmp = (*iterator)->findObject(Node);
			if (tmp != NULL){ return tmp; }
		}
	}
	return NULL;
}

void SceneObject::addChildObject(SceneObject* childObject) // OK
{
	if (childObject != NULL)
	{
		childObject->setNewParent(this);
		_childObjects.push_back(childObject);
	}
}

void SceneObject::setNewParent(SceneObject* parentObject)
{
	if (_parent != NULL){
		_parent->detachChild(this);
	}
	this->_parent = parentObject;
}

bool SceneObject::validate()
{
	bool state = _type != 0 && !_date.name.empty() && !_date.meshName.empty() && _SceneNode!=NULL;
	return state;
}

SceneObject* SceneObject::getParent()
{
	return _parent;
}

void SceneObject::detachChild(SceneObject* object)
{
	auto iterator = _childObjects.begin();
	for ( ; iterator != _childObjects.end(); iterator++)
	{
		if ((*iterator) == object){
			(*iterator)->setNewParent(NULL);
			_childObjects.erase(iterator);
			return;
		}
	}
}

bool SceneObject::compareSceneNode(Ogre::SceneNode* node)
{
	if (_SceneNode == node) return true;
	else return false;
}

MeshDate SceneObject::getMeshDate()
{
	return _date;
}

void SceneObject::setNewDate(MeshDate date)
{
	_date.position = date.position;
	this->_SceneNode->setPosition(_date.position);
	if (_date.type == EntityMask){
		_date.scale = date.scale;
		this->_SceneNode->setScale(_date.scale);

	}
	if (_date.type == EntityMask || _date.type == LightsMasks){
		_date.orientatnion = date.orientatnion;
		_SceneNode->setOrientation(_date.orientatnion);
	}
	if (_date.type == LightsMasks){
		if (Ogre::Light* l = dynamic_cast<Ogre::Light*>(_SceneNode->getAttachedObject(_SceneNode->getName()))){
			_date.diffuseColor = date.diffuseColor;
			l->setDiffuseColour(_date.diffuseColor);
			_date.specularColor = date.specularColor;
			l->setSpecularColour(_date.specularColor);
			if (_date.meshName.compare("LT_SPOTLIGHT") == 0){
				_date.spotlight = date.spotlight;
				l->setSpotlightRange(Ogre::Radian(Ogre::Degree(_date.spotlight.x)), Ogre::Radian(Ogre::Degree(_date.spotlight.y)));
			}
		}
	}
	if (date.name.length() > 3 && date.name.compare(_date.name) != 0){
		_date.name = date.name;
	}
}

void SceneObject::Update()
{
	_date.position = _SceneNode->getPosition();
	_date.orientatnion = _SceneNode->getOrientation();
	_date.scale = _SceneNode->getScale();
}

void SceneObject::CreateXmlSceneNode(TiXmlElement* rootNode)
{
	if (rootNode != NULL){
			TiXmlElement* node = new TiXmlElement("node");
			node->SetAttribute("name", _date.name.c_str());

			auto position = new TiXmlElement("position");
			position->SetAttribute("x", Ogre::StringConverter::toString(_date.position.x).c_str());
			position->SetAttribute("y", StringConverter::toString(_date.position.y).c_str());
			position->SetAttribute("z", StringConverter::toString(_date.position.z).c_str());
			node->LinkEndChild(position);

			if (_date.type == EntityMask || _date.type == LightsMasks){
				auto rotation = new TiXmlElement("rotation");
				rotation->SetAttribute("qx", Ogre::StringConverter::toString(_date.orientatnion.x).c_str());
				rotation->SetAttribute("qy", StringConverter::toString(_date.orientatnion.y).c_str());
				rotation->SetAttribute("qz", StringConverter::toString(_date.orientatnion.z).c_str());
				rotation->SetAttribute("qw", StringConverter::toString(_date.orientatnion.w).c_str());
				node->LinkEndChild(rotation);
			}
			if (_date.type == EntityMask){
				auto scale = new TiXmlElement("scale");
				scale->SetAttribute("x", Ogre::StringConverter::toString(_date.scale.x).c_str());
				scale->SetAttribute("y", StringConverter::toString(_date.scale.y).c_str());
				scale->SetAttribute("z", StringConverter::toString(_date.scale.z).c_str());
				node->LinkEndChild(scale);

				auto entity = new TiXmlElement("entity");
				entity->SetAttribute("name", _date.name.c_str());
				entity->SetAttribute("meshFile", _date.meshName.c_str());
				entity->SetAttribute("static", "true");
				node->LinkEndChild(entity);
			}
			else if (_date.type == ParticlesMasks){
				auto particleSystem = new TiXmlElement("particleSystem");
				particleSystem->SetAttribute("name", _date.name.c_str());
				particleSystem->SetAttribute("file", _date.meshName.c_str());
				node->LinkEndChild(particleSystem);
			}
			else if (_date.type == LightsMasks){
				auto light = new TiXmlElement("light");
				light->SetAttribute("name", getLightType(_date.meshName).c_str());
				node->LinkEndChild(light);
				auto diffColor = new TiXmlElement("colourDiffuse");
				diffColor->SetAttribute("r", StringConverter::toString(_date.diffuseColor.r).c_str());
				diffColor->SetAttribute("g", StringConverter::toString(_date.diffuseColor.g).c_str());
				diffColor->SetAttribute("b", StringConverter::toString(_date.diffuseColor.b).c_str());
				light->LinkEndChild(diffColor);

				auto specColor = new TiXmlElement("colourSpecular");
				specColor->SetAttribute("r", StringConverter::toString(_date.specularColor.r).c_str());
				specColor->SetAttribute("g", StringConverter::toString(_date.specularColor.g).c_str());
				specColor->SetAttribute("b", StringConverter::toString(_date.specularColor.b).c_str());
				light->LinkEndChild(specColor);
				if (_date.meshName.compare("LT_SPOTLIGHT") == 0){
					auto spotRange = new TiXmlElement("lightRange");
					spotRange->SetAttribute("inner", StringConverter::toString(_date.spotlight.x).c_str());
					spotRange->SetAttribute("outer", StringConverter::toString(_date.spotlight.y).c_str());
					light->LinkEndChild(spotRange);
				}
			}

		auto iterator = this->_childObjects.begin();
		for (; iterator != _childObjects.end(); iterator++){
			(*iterator)->CreateXmlSceneNode(node);
		}

		rootNode->LinkEndChild(node);
	}
}

Ogre::String SceneObject::getLightType(Ogre::String type)
{
	if (type.compare("LT_SPOTLIGHT") == 0){
		return "spot";
	}
	else if (type.compare("LT_DIRECTIONAL") == 0){
		return "directional";
	}
	else
		return "point";
}
