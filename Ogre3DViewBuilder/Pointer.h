#pragma once
#include "OgreEntity.h"
#include "OgreSceneNode.h"
#include "OgreVector3.h"
class Pointer
{
public:
	Pointer(Ogre::SceneNode* pointerNode);
	~Pointer();
	Ogre::Vector3 getPointerPosition();
	void setPointerPosition(Ogre::Vector3 newPosition);
private:
	Ogre::SceneNode* _pointerNode;
};

