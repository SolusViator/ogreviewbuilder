#include "LightCreateViewPage.h"

using Ogre::StringConverter;

LightCreateViewPage::LightCreateViewPage(GuiController* guiController)
{
	MyGUI::Gui* gui = MyGUI::Gui::getInstancePtr();
	PageName = "LightCreateViewPage";
	_PositionX = initialiseByAttribute<MyGUI::EditBox>("LightPositionX");
	_PositionY = gui->findWidget<MyGUI::EditBox>("LightPositionY");
	_PositionZ = gui->findWidget<MyGUI::EditBox>("LightPositionZ");
	_lightsComboBox = gui->findWidget<MyGUI::ComboBox>("LightType");
	_lightsComboBox->eventComboChangePosition = MyGUI::newDelegate(this, &LightCreateViewPage::_showSpotlightProperties);

	_DiffRed = gui->findWidget<MyGUI::EditBox>("LightDiffColorR");
	_DiffGreen = gui->findWidget<MyGUI::EditBox>("LightDiffColorG");
	_DiffBlue = gui->findWidget<MyGUI::EditBox>("LightDiffColorB");
	_DiffColor = gui->findWidget<MyGUI::Widget>("LightDiffColor");
	_DiffColor->eventMouseButtonClick = MyGUI::newDelegate(this, &LightCreateViewPage::DiffColorButtonPressed);

	_SpecRed = gui->findWidget<MyGUI::EditBox>("LightSpecColorR");
	_SpecGreen = gui->findWidget<MyGUI::EditBox>("LightSpecColorG");
	_SpecBlue = gui->findWidget<MyGUI::EditBox>("LightSpecColorB");
	_SpecColor = gui->findWidget<MyGUI::Widget>("LightSpecColor");
	_SpecColor->eventMouseButtonClick = MyGUI::newDelegate(this, &LightCreateViewPage::SpecColorButtonPressed);

	_SpecMid = initialiseByAttribute<MyGUI::EditBox>("SpotlightMiddle");
	_SpecOut = initialiseByAttribute<MyGUI::EditBox>("SpotlightOutside");
	_SpecWidget = initialiseByAttribute<MyGUI::Widget>("SpotlightCreateProp");

	gui->findWidget<MyGUI::Button>("LightCreateButton")->eventMouseButtonClick =
		MyGUI::newDelegate(guiController, &GuiController::CreateLightButtonClicked);
	gui->findWidget<MyGUI::Button>("LightCursorPosition")->eventMouseButtonClick =
		MyGUI::newDelegate(this, &LightCreateViewPage::setCursorPosition);

	_ColorPanel = ColorPanelViewPage::getInstancePtr();
	_ColorPanel->HideGuiWindow();
}


LightCreateViewPage::~LightCreateViewPage()
{
}

void LightCreateViewPage::ShowGuiWindow()
{
}
void LightCreateViewPage::HideGuiWindow()
{
}

MeshDate LightCreateViewPage::getMeshDate()
{
	float x = 1.f / 255.f;
	Ogre::Vector3 position;
	Ogre::ColourValue specColor, diffColor;
	position.x = StringConverter::parseReal(_PositionX->getCaption());
	position.y = StringConverter::parseReal(_PositionY->getCaption());
	position.z = StringConverter::parseReal(_PositionZ->getCaption());
	specColor = changeMyGUItoOgreColour(getSpecColour());
	diffColor = changeMyGUItoOgreColour(getDiffColour());
	Ogre::String LightName = _lightsComboBox->getCaption();
	Ogre::Vector2 SpotlightDate;
	if (LightName.compare("LT_SPOTLIGHT") == 0){
		SpotlightDate.x = StringConverter::parseReal(_SpecMid->getCaption());
		SpotlightDate.y = StringConverter::parseReal(_SpecOut->getCaption());
		if (SpotlightDate.x < 0 || SpotlightDate.x > 180)SpotlightDate.x = 30.0;
		if (SpotlightDate.y < 0 || SpotlightDate.y > 180)SpotlightDate.y = 30.0;
	}
	MeshDate date;
	date.meshName = LightName;
	date.orientatnion = Ogre::Quaternion::IDENTITY;
	date.scale = Ogre::Vector3(1, 1, 1);
	date.position = position;
	date.type = LightsMasks;
	date.specularColor = specColor;
	date.diffuseColor = diffColor;
	date.spotlight = SpotlightDate;
	return date;
}

void LightCreateViewPage::setCursorPosition(MyGUI::Widget* _widget)
{
	Ogre::Vector3 pointerPosition = PointerController::getPointerPosition();
	_PositionX->setCaption(Ogre::StringConverter::toString(pointerPosition.x));
	_PositionY->setCaption(Ogre::StringConverter::toString(pointerPosition.y));
	_PositionZ->setCaption(Ogre::StringConverter::toString(pointerPosition.z));
}

void LightCreateViewPage::DiffColorButtonPressed(MyGUI::Widget* _sender)
{
	_ColorPanel->setColour(getDiffColour());
	_ColorPanel->setChangedWidgets(_DiffRed, _DiffGreen, _DiffBlue, _DiffColor);
	_ColorPanel->ShowGuiWindow();
}
void LightCreateViewPage::SpecColorButtonPressed(MyGUI::Widget* _sender)
{
	_ColorPanel->setColour(getSpecColour());
	_ColorPanel->setChangedWidgets(_SpecRed, _SpecGreen, _SpecBlue, _SpecColor);
	_ColorPanel->ShowGuiWindow();
}

void LightCreateViewPage::_showSpotlightProperties(MyGUI::Widget* _sender, size_t _index)
{
	try{
		if (_lightsComboBox){
			std::string x = _lightsComboBox->getItemNameAt(_index);
			bool tmp = x.compare("LT_SPOTLIGHT") == 0;
			if (tmp){
				_SpecWidget->setVisible(true);
			}
			else{
				_SpecWidget->setVisible(false);
			}
		}
	}
	catch (...){}
}


MyGUI::Colour LightCreateViewPage::getDiffColour()
{
	float x = 1.f / 255.f;
	MyGUI::Colour diffColor;
	diffColor.red = x*StringConverter::parseReal(_DiffRed->getCaption());
	diffColor.green = x*StringConverter::parseReal(_DiffGreen->getCaption());
	diffColor.blue = x*StringConverter::parseReal(_DiffBlue->getCaption());
	return diffColor;
}
MyGUI::Colour LightCreateViewPage::getSpecColour()
{
	float x = 1.f / 255.f;
	MyGUI::Colour specColor;
	specColor.red = x*StringConverter::parseReal(_SpecRed->getCaption());
	specColor.green = x*StringConverter::parseReal(_SpecGreen->getCaption());
	specColor.blue = x*StringConverter::parseReal(_SpecBlue->getCaption());
	return specColor;
}
Ogre::ColourValue LightCreateViewPage::changeMyGUItoOgreColour(MyGUI::Colour colour)
{
	return Ogre::ColourValue(colour.red, colour.green, colour.blue);
}