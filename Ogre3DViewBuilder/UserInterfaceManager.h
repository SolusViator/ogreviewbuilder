#pragma once
#include "MYGUI\MyGUI.h"
#include "MYGUI\MyGUI_OgrePlatform.h"
#include "GuiController.h"
#include "ResourcesList.h"
#include "MeshManager.h"
#include "MeshCreateViewPage.h"
#include "ParticleCreateViewPage.h"
#include "LightCreateViewPage.h"
#include "SceneManager.h"

class UserInterfaceManager
{
public:
	UserInterfaceManager(Ogre::RenderWindow* mWindow, Ogre::SceneManager* mSceneManager, GuiController*, ResourcesList*, MeshManager*);
	MyGUI::Gui* getMyGUIRoot();
	MyGUI::OgrePlatform* getMyGUIPlatfrom();
	void updateGui();
	~UserInterfaceManager();
protected:
	MyGUI::Gui* _mGui;
	MyGUI::OgrePlatform* _mPlatform;
	GuiController* _guiController;
	MeshManager* _meshManager;
	MeshCreateViewPage* _meshCreatePage;
	ParticleCreateViewPage* _particleCreatePage;
	LightCreateViewPage* _lightCreatePage;
};

