#include "Camera.h"

using Ogre::Radian;
using Ogre::Quaternion;

Camera::Camera(Ogre::SceneManager* mSceneManager, Ogre::RenderWindow* mWindow)
{
	_mSceneManager = mSceneManager;
	_mWindow = mWindow;
}


Camera::~Camera()
{
}

bool Camera::CreateCamera(Ogre::String name, Ogre::ColourValue backgroundColor, float nearDistance, float farDistance, Ogre::Real aspectRatio)
{
	try{
		_mCamera = _mSceneManager->createCamera(name);
		_mCameraNode = _mSceneManager->getRootSceneNode()->createChildSceneNode(name);
		_mCameraNode->attachObject(_mCamera);
		Ogre::Viewport *vp = _mWindow->addViewport(_mCamera);
		vp->setBackgroundColour(backgroundColor);
		if (aspectRatio != 0){
			_mCamera->setAspectRatio(aspectRatio);
		}
		else{
			_mCamera->setAspectRatio((Ogre::Real(vp->getActualWidth()) / Ogre::Real(vp->getActualHeight())));
		}
		_mCamera->setNearClipDistance(nearDistance);
		_mCamera->setFarClipDistance(farDistance);
		return true;
	}
	catch (...){
		return false;
	}
}

void Camera::enableCompositor(Ogre::String comName)
{
	Ogre::CompositorManager::getSingleton().setCompositorEnabled(_mCamera->getViewport(), comName, true);
}
void Camera::disableCompositor(Ogre::String comName)
{
	Ogre::CompositorManager::getSingleton().setCompositorEnabled(_mCamera->getViewport(), comName, false);
}

Ogre::Vector3 Camera::getCameraDirection()
{
	return _mCamera->getDerivedDirection();
}

Ogre::Vector3 Camera::getCameraPosition()
{
	return _mCamera->getDerivedPosition();
}

Ogre::Ray Camera::getCameraRay()
{
	return Ogre::Ray(getCameraPosition(), getCameraDirection());
}

Ogre::Ray Camera::getCameraToViewportRay(float screenX, float screenY)
{
	return _mCamera->getCameraToViewportRay(screenX / float(_mWindow->getWidth()), screenY / float(_mWindow->getHeight()));

}

void Camera::setCameraDirection(Ogre::Vector3 direction)
{
	_mCameraNode->setDirection(direction);
}
void Camera::setCameraRelDirection(Ogre::Vector3 direction)
{
	_mCameraNode->setDirection(getCameraDirection() + direction);
}

void Camera::setCameraPosition(Ogre::Vector3 position)
{
	_mCameraNode->setPosition(position);
}
void Camera::setCameraRelPosition(Ogre::Vector3 position)
{
	_mCameraNode->translate(position);
}

void Camera::setCameraRelRotation(Ogre::Quaternion angle)
{
	_mCameraNode->setOrientation(_mCameraNode->getOrientation()*angle);
}

void Camera::lookCameraAt(Ogre::Vector3 point)
{
	_mCameraNode->lookAt(point, Ogre::Node::TS_WORLD);
}

Ogre::Quaternion Camera::getCameraOrientation()
{
	return _mCameraNode->getOrientation();
}

void Camera::setCameraOrientation(Ogre::Quaternion orientation)
{
	Ogre::LogManager::getSingleton().logMessage("CameraNodeOrientation: " + Ogre::StringConverter::toString(_mCameraNode->getOrientation()) + "\n");
	_mCameraNode->_setDerivedOrientation(orientation);
}

void Camera::setCameraPitch(Ogre::Radian rad)
{
	_mCameraNode->pitch(rad);
}
void Camera::setCameraYaw(Ogre::Radian rad)
{
	_mCameraNode->yaw(rad);
}
void Camera::setCameraRoll(Ogre::Radian rad)
{
	_mCameraNode->roll(rad);
}