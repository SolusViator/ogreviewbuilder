#include "UserInterfaceManager.h"


UserInterfaceManager::UserInterfaceManager(Ogre::RenderWindow* mWindow, Ogre::SceneManager* mSceneManager, 
										   GuiController* guiController, ResourcesList* resList,
										   MeshManager* meshManager)
{
	_mPlatform = new MyGUI::OgrePlatform();
	_mPlatform->initialise(mWindow, mSceneManager); // mWindow is Ogre::RenderWindow*, mSceneManager is Ogre::SceneManager*
	_mGui = new MyGUI::Gui();
	_mGui->initialise();
	_meshManager = meshManager;
	_guiController = guiController;
	_meshCreatePage = new MeshCreateViewPage(resList, guiController);
	_particleCreatePage = new ParticleCreateViewPage(resList, guiController);
	_lightCreatePage = new LightCreateViewPage(guiController);
}


UserInterfaceManager::~UserInterfaceManager()
{
	_mGui->shutdown();
	delete _mGui;
	_mGui = 0;
	_mPlatform->shutdown();
	delete _mPlatform;
	_mPlatform = 0;
}

MyGUI::Gui* UserInterfaceManager::getMyGUIRoot()
{
	return _mGui;
}

MyGUI::OgrePlatform* UserInterfaceManager::getMyGUIPlatfrom()
{
	return _mPlatform;
}

void UserInterfaceManager::updateGui()
{
	unsigned int changeViewPageMask = _guiController->getMask(ChangePageViewMask);
	if (changeViewPageMask){
		if (changeViewPageMask&MeshCreatePage){
			if (_meshCreatePage->isVisible())_meshCreatePage->HideGuiWindow();
			else _meshCreatePage->ShowGuiWindow();
		}
	}

	_guiController->removeMask(ChangePageViewMask, (~0));

	unsigned int createObjectMask = _guiController->getMask(CreateObjectMask);
	if (createObjectMask){
		Ogre::SceneNode* node = NULL;
		MeshDate date;
		if (createObjectMask&Mesh){
			date = _meshCreatePage->getMeshDate();
			node = _meshManager->createEntityWithoutNode(date.meshName,date.position, date.orientatnion, date.scale);
		}
		else if (createObjectMask&ParticleEffect){
			date = _particleCreatePage->getParticleDate();
			node = _meshManager->createParticleSystem(date.meshName, date.position);
		}
		else if (createObjectMask&LightsMasks)
		{
			date = _lightCreatePage->getMeshDate();
			node = _meshManager->createLight(date.meshName, date.position, date.specularColor, date.diffuseColor);
		}
		if (node != NULL){
			date.name = node->getName();
			date.ObjectNode = node;
			SceneManager::getSingleton().addNewObject(new SceneObject(date));
		}
	}
	_guiController->removeMask(CreateObjectMask, (~0));
}
