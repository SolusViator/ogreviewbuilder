#include "Engine.h"

#ifdef _DEBUG
#  define PRINTF printf
#else
#  define PRINTF //printf
#endif


Engine::Engine(void) : _mRoot(0),
mResourcesCfg(Ogre::StringUtil::BLANK),
mPluginsCfg(Ogre::StringUtil::BLANK)
{
#ifdef _DEBUG
	mResourcesCfg = "resources_d.cfg";
	mPluginsCfg = "plugins_d.cfg";
#else
	mResourcesCfg = "resources.cfg";
	mPluginsCfg = "plugins.cfg";
#endif
}


Engine::~Engine(void)
{
}

bool Engine::initialise()
{
	if (!InitialiseEngine()) return false;
	InitialiseOIS();
	return true;

}

void Engine::InitialiseOIS()
{
	OIS::ParamList pl;
	size_t windowHnd = 0;
	std::ostringstream windowHndStr;

	_mWindow->getCustomAttribute("WINDOW", &windowHnd);
	windowHndStr << windowHnd;
	pl.insert(std::make_pair(std::string("WINDOW"), windowHndStr.str()));

	_mInputManager = OIS::InputManager::createInputSystem(pl);
	_mKeyboard = static_cast<OIS::Keyboard*>(_mInputManager->createInputObject(OIS::OISKeyboard, true));
	_mMouse = static_cast<OIS::Mouse*>(_mInputManager->createInputObject(OIS::OISMouse, true));

	const OIS::MouseState &mouseState = _mMouse->getMouseState();
	mouseState.width = _mWindow->getWidth(); // your rendering area width
	mouseState.height = _mWindow->getHeight(); // your rendering area height

}

bool Engine::InitialiseEngine()
{
	_mRoot = new Ogre::Root(mPluginsCfg, "MyPlace.cfg", "MyPlace.log");
	/****************�adowanie lokalizacji plik�w******************/
	mConfigFile.load(mResourcesCfg);
	Ogre::String name, locType;
	Ogre::ConfigFile::SectionIterator secIt = mConfigFile.getSectionIterator();
	while (secIt.hasMoreElements())
	{
		Ogre::ConfigFile::SettingsMultiMap * settings = secIt.getNext();
		Ogre::ConfigFile::SettingsMultiMap::iterator it;
		for (it = settings->begin(); it != settings->end(); ++it)
		{
			locType = it->first;
			name = it->second;
			Ogre::ResourceGroupManager::getSingleton().addResourceLocation(name, locType);
		}
	}
	/****************Konfigurowanie Systemu Renderowania**************/
	if (!_mRoot->showConfigDialog())
		return false;

	_mWindow = _mRoot->initialise(true, "MyPlace");

	Ogre::TextureManager::getSingleton().setDefaultNumMipmaps(3);
	Ogre::ResourceGroupManager::getSingleton().initialiseAllResourceGroups();
	_mSceneMgr = _mRoot->createSceneManager(Ogre::ST_GENERIC);

	return true;
}

Ogre::SceneManager* Engine::getSceneManager(){
	return _mSceneMgr;
}
Ogre::RenderWindow* Engine::getRenderWindow(){
	return _mWindow;
}
OIS::Mouse* Engine::getMouse(){
	return _mMouse;
}
OIS::Keyboard* Engine::getKeyboard(){
	return _mKeyboard;
}
bool Engine::renderFrame(){
	_Timer.reset();
	Ogre::WindowEventUtilities::messagePump();
	if (_mWindow->isClosed()) return false;
	if (!_mRoot->renderOneFrame()) return false;
	return true;
}
void Engine::captureInputDevices(){
	_mKeyboard->capture();
	_mMouse->capture();
}

float Engine::getTimeSinceLastFrame()
{
	return (_Timer.getMilliseconds()*0.001);
}

/*
bool Engine::_gameStory()
{
	while (true)
	{
		float time = _timeSinceLastFrame.getMilliseconds()*0.001;
		if (time >= (1.f / 60.f)){
			_timeSinceLastFrame.reset();
			mKeyboard->capture();
			mMouse->capture();
			Ogre::WindowEventUtilities::messagePump();
			if (mWindow->isClosed()) return false;
			if (!mRoot->renderOneFrame()) return false;
		}
	}
}
*/