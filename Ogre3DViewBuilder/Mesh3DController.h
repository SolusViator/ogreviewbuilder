#pragma once
#include "Ogre.h"
#include "ObjectsMask.h"
#include "EditedObject.h"
#include <vector>
struct Axes{
	enum  Direction{
		X = 0,
		Y = 1,
		Z = 2,
		NoN = 7
	};

	enum  Type{
		Move = 0,
		Rotate = 1,
		Scale = 2,
		Null = 7
	};
};

class Mesh3DController
{
public:
	static Mesh3DController& getSingleton();
	void Update();
	void setVisibleType(Axes::Type type);
	Axes::Type getVisibleType(){ return activeType; }
	void setBaseSceneNode(Ogre::SceneNode*);
	void setMoveCursors(Ogre::Entity* XAxis, Ogre::Entity* YAxis, Ogre::Entity* ZAxis);
	void setRotateCursors(Ogre::Entity* XAxis, Ogre::Entity* YAxis, Ogre::Entity* ZAxis);
	void setScaleCursors(Ogre::Entity* XAxis, Ogre::Entity* YAxis, Ogre::Entity* ZAxis);
	Axes::Direction getAxe(Axes::Type type, Ogre::SceneNode* node);
private:
	Ogre::SceneNode* _parentSceneNode;
	static Mesh3DController* _instance;
	Axes::Type activeType;
	std::vector<Ogre::SceneNode*> _MoveCursors;
	std::vector<Ogre::SceneNode*> _RotateCursors;
	std::vector<Ogre::SceneNode*> _ScaleCursors;
	Mesh3DController();
	~Mesh3DController();
	void setCursorsScale(Ogre::Vector3 size);
	void hide(Axes::Type type);
	void show(Axes::Type type);
};

