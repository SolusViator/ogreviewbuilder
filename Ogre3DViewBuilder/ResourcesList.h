#pragma once
#include <list>
#include "Ogre.h"
#include "tinyxml.h"
class ResourcesList
{
public:

	bool parseConfigFile(Ogre::String fileName);
	ResourcesList(){

	}
	~ResourcesList(){

	}

	std::list<Ogre::String> meshes;
	std::list<Ogre::String> materials;
	std::list<Ogre::String> particles;
protected:
	void loadFile(TiXmlElement* XMLRoot);
	void loadMeshInfo(TiXmlElement* pElement);
	void loadMaterialInfo(TiXmlElement* pElement);
	void loadParticleInfo();

};

