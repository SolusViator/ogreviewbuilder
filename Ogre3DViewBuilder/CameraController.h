#pragma once
#include "Camera.h"
#include "OISKeyboard.h"
#include "OISMouse.h"

enum KeyboardStates
{
	moveForward = 1,
	moveBackward = 1 << 1,
	moveLeft = 1<<2,
	moveRight = 1<<3,
	resetPosition = 1<<4
};

enum MouseStates
{
	rotateLeft = 1,
	rotateRight = 1 << 1,
	rotateUp = 1 << 2,
	rotateDown = 1 << 3,
	releaseMouse = 1<<4,
	moveUp = 1 << 5,
	moveDown = 1 << 6
};

class CameraController
{
public:
	CameraController(Camera* camera);
	~CameraController();
	virtual void UpdateCamera();
	void setKeyboardMaskBit(unsigned int state);
	void removeKeyboardMaskBit(unsigned int state);
	void setMouseMaskBit(unsigned int state);
	void removeMouseMaskBit(unsigned int state);
	Ogre::Ray getCameraToViewportRay(float positionX, float positionY);
	Ogre::Quaternion getCameraOrientatnion();
	Ogre::Vector3 getCameraPosition();
	Ogre::Vector3 getCameraDirection();
	void setMouseRelDistance(Ogre::Vector2 distance);
protected:
	unsigned int _MovementMask;
	unsigned int _MouseMask;
	float _mouseXDistance;
	float _mouseYDistance;
	Camera* _mCamera;
	void MoveCameraForward(float distance);
	void MoveCameraBackward(float distance);
	void MoveCameraLeft(float distance);
	void MoveCameraRight(float distance);
	void RotateCamera(float angle, Ogre::Vector3 axes);
	void MoveCamera(Ogre::Vector3 direction, float distance);
	void ResetPosition();
};
