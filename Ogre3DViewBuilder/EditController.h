#pragma once
#include "Ogre.h"
#include "PointerController.h"
#include "MaskController.h"
#include "MeshEditViewPage.h"
#include "Mesh3DController.h"
#include "SceneManager.h"
class EditController : public MaskController
{
public:
	EditController(PointerController* pointerController);
	void ChangeEditingObject(Ogre::SceneNode* node, Ogre::Ray ray, Ogre::Vector3 hitPoint);
	void Update(Ogre::Quaternion cameraOrientatnion);
	~EditController();
	void LeftButtonPressed(Ogre::SceneNode*);
	void setManipulatorType(Axes::Type type){ this->type = type; }
	void LeftButtonRelease();
	void setMouseRelPosition(Ogre::Vector2 relPosition);
protected:
	PointerController* _pointerController;
	MeshEditViewPage* _MeshEditViewPage;
	bool isLeftButtonPressed;
	Axes::Direction direction;
	Axes::Type type;
	Ogre::Vector2 relMousePosition;
};

