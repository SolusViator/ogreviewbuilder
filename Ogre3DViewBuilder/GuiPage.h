#pragma once
#include "OgreString.h"
#include "MYGUI\MyGUI.h"
class GuiPage
{
public:
	GuiPage(){

	}
	virtual ~GuiPage(){

	}
	virtual void ShowGuiWindow() = 0;
	virtual void HideGuiWindow() = 0;
	/*You can override this method if you need to change GUI state in every frame */
	virtual void Update(){}
	Ogre::String getPageName(){ return PageName; }
	bool isVisible(){ return _isVisible; }
protected:
	Ogre::String PageName;
	bool _isVisible;


};

