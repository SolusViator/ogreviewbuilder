#pragma once
#include "Ogre.h"
#include "OISEvents.h"
#include "OISInputManager.h"
#include "OISKeyboard.h"
#include "OISMouse.h"

class Engine
{
private:
	Ogre::Root *_mRoot;
	Ogre::RenderWindow *_mWindow;
	Ogre::SceneManager *_mSceneMgr;
	Ogre::String mResourcesCfg;
	Ogre::String mPluginsCfg;
	Ogre::ConfigFile mConfigFile;
	OIS::InputManager *_mInputManager;
	OIS::Mouse *_mMouse;
	OIS::Keyboard *_mKeyboard;

public:
	Engine(void);
	~Engine(void);
	bool initialise();
	Ogre::SceneManager* getSceneManager();
	Ogre::RenderWindow* getRenderWindow();
	OIS::Mouse* getMouse();
	OIS::Keyboard* getKeyboard();
	bool renderFrame();
	void captureInputDevices();
	float getTimeSinceLastFrame();
protected:
	void CreateRenderWindow();
	void InitialiseOIS();
	bool InitialiseEngine();
	void InitialiseManagers();
	Ogre::Timer _Timer;
};

