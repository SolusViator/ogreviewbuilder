#pragma once
#include "GuiPage.h"
#include "MeshDate.h"
#include "PointerController.h"
#include "Ogre.h"
#include "GuiController.h"
#include "ColorPanelViewPage.h"
class LightCreateViewPage : public GuiPage
{
public:
	virtual void ShowGuiWindow();
	virtual void HideGuiWindow();
	LightCreateViewPage(GuiController* guiController);
	MeshDate getMeshDate();
	~LightCreateViewPage();
private:
	MyGUI::EditBox* _PositionX;
	MyGUI::EditBox* _PositionY;
	MyGUI::EditBox* _PositionZ;
	MyGUI::ComboBox* _lightsComboBox;

	MyGUI::EditBox* _DiffRed;
	MyGUI::EditBox* _DiffGreen;
	MyGUI::EditBox* _DiffBlue;
	MyGUI::Widget* _DiffColor;

	MyGUI::EditBox* _SpecRed;
	MyGUI::EditBox* _SpecGreen;
	MyGUI::EditBox* _SpecBlue;
	MyGUI::Widget* _SpecColor;

	MyGUI::EditBox* _SpecMid;
	MyGUI::EditBox* _SpecOut;
	MyGUI::Widget* _SpecWidget;

	ColorPanelViewPage* _ColorPanel;

	void setCursorPosition(MyGUI::Widget* _widget);

	void DiffColorButtonPressed(MyGUI::Widget* _sender);
	void SpecColorButtonPressed(MyGUI::Widget* _sender);
	void _showSpotlightProperties(MyGUI::Widget* _sender, size_t _index);
	MyGUI::Colour getDiffColour();
	MyGUI::Colour getSpecColour();
	Ogre::ColourValue changeMyGUItoOgreColour(MyGUI::Colour colour);
	template<typename T>
	T* initialiseByAttribute(std::string name)
	{
		return MyGUI::Gui::getInstancePtr()->findWidget<T>(name);
	}
};

